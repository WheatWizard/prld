 # prld

`Prld` is a replacement for haskells `Prelude` library designed for golfing.

 ## Cheatsheet

This cheatsheet provides a list of all of the functions and type classes present in base so that you can find the proper replacement in `Prld`.

| base object    | Fate                                                     |
| ---------------| -------------------------------------------------------  |
| `all`          | Now named `a`.                                           |
| `and`          | Not yet implemented.                                     |
| `concat`       | Replaced by the more general `j`.                        |
| `const`        | Replaced by the more general `p`.                        |
| `curry`        | Replaced by the more general `uc`.                       |
| `drop`         | Replaced by the more general `d`.                        |
| `elem`         | Replaced by the more general `e`.                        |
| `fix`          | Not yet implemented                                      |
| `flip`         | Now named `f`.                                           |
| `fmap`         | Now named `m`, `(.)` for infix.                          |
| `fst`          | Not yet implemented                                      |
| `group`        | Now named `g`.                                           |
| `head`         | Replaced with `h`.                                       |
| `id`           | Now named `($)`.                                         |
| `iterate`      | Now named `x`.                                           |
| `join`         | Replaced by the more general `j`.                        |
| `length`       | Replaced by the more general `l`.                        |
| `liftA2`       | Replaced by the more general `l2`. In some cases use `z` |
| `map`          | Replaced by the more general `m`, `(.)` for infix.       |
| `mappend`      | Replaced by `(<>)`.                                      |
| `max`          | Not yet implemented.                                     |
| `mempty`       | Now named `k`.                                           |
| `min`          | Not yet implemented.                                     |
| `not`          | Now named `n`.                                           |
| `notElem`      | Replaced by the more general `ne`.                       |
| `or`           | Not yet implemented.                                     |
| `pure`         | Replaced by the more general `p`.                        |
| `repeat`       | Replaced by the more general `p`.                        |
| `return`       | Replaced by the more general `p`.                        |
| `reverse`      | Replaced by the more general `r`.                        |
| `snd`          | Not yet implemented.                                     |
| `sort`         | Not yet implemented.                                     |
| `tail`         | Not yet implemented.                                     |
| `take`         | Replaced by the more general `t`.                        |
| `uncurry`      | Replaced by the more general `c`.                        |
| `unzip`        | Replaced by the more general `uz`.                       |
| `zip`          | Replaced by the more general `z'`.                       |
| `zipWith`      | Replaced by the more general `z`.                        |
| `(.)`          | Generalized.                                             |
| `($)`          | Generalized.                                             |
| `(+)`          | No change.                                               |
| `(-)`          | No change.                                               |
| `(<)`          | No change.                                               |
| `(>)`          | Not yet implemented.                                     |
| `(:)`          | No change.                                               |
| `(<>)`         | No change.                                               |
| `(++)`         | Generalized.                                             |
| `(==)`         | No change.                                               |
| `($>)`         | Not yet implemented.                                     |
| `(<$)`         | Not yet implemented.                                     |
| `(*>)`         | Not yet implemented.                                     |
| `(>>)`         | Not yet implemented.                                     |
| `(<*>)`        | Generalized.                                             |
| `(>>=)`        | Generalized.                                             |
| `(=<<)`        | Generalized.                                             |
| `(<$>)`        | Now named `(.)`, `m` for prefix.                         |

