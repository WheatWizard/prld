{-# Language InstanceSigs #-}
{-# Language FlexibleContexts #-}
{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
{-# Language UndecidableSuperClasses #-}
{-# Language MultiParamTypeClasses #-}
{-# Language FunctionalDependencies #-}
module Prld
  ( module Prld.Containers
  , module Prld.Monad
  , module Prld.Bool
  , module Prld.Distributive
  , module Prld.Coerce
  , module Prld.Function
  , module Prld.Flip
  , Curriable
    ( uc
    , c
    , tT
    , dT
    )
  , Semigroup
    ( (<>)
    )
  , Monoid
    (
    )
  , g
  , k
  , pr
  , x, fx
  , (-)
  , ($)
  , (==)
  , (|>)
  , IO
  -- * Zipping
  , Zippable
    ( z
    , zi
    )
  , DefaultZippable
    ( zd
    )
  , z'
  , zI
  , zp
  , zP
  , zs
  , zm
  )
  where

import qualified Prelude
import Prelude
  ( Num
  , Foldable
  , Eq
  , Ord
  , Integral
  , Show
  , Read
  , IO
  , Semigroup
    ( (<>)
    )
  , Monoid
    ( mempty
    )
  )
import qualified Control.Monad as Monad
import qualified Control.Applicative as Applicative
import qualified Data.List as List
import qualified Data.Foldable as Foldable

import Prld.Containers
import Prld.Monad
import Prld.Bool
import Prld.Distributive
import Prld.Coerce
import Prld.Function
import Prld.Flip

(-) :: Num a => a -> a -> a
(-) = (Prelude.-)


(==) :: Eq a => a -> a -> Prelude.Bool
(==) = (Prelude.==)

(<) :: Ord a => a -> a -> Prelude.Bool
(<) = (Prelude.<)

-- | An identity function.
-- Works like '(Prelude.$)' when infixed.
($) :: a -> a
($) = Prelude.id
infixr 0 $

-- | The 'Prelude.flip' of '($)'
-- This operator is used in Elm's Prelude.
(|>) :: a -> (a -> b) -> b
x |> f = f x

-- | For structures we can curry over.
-- Note that because 'Prelude.uncurry' is much more common than 'Prelude.curry',
-- we have named it 'c' and 'Prelude.curry' 'uc'.
-- These should not be thought of as abreviations. 
class Curriable f where
  {-# Minimal uc, c | tT, dT #-}
  -- | General version of 'Prelude.curry'.
  uc :: (f a b -> c) -> a -> b -> c
  uc f = Prelude.curry (f . dT)
  -- | General version of 'Prelude.uncurry'.
  c :: (a -> b -> c) -> f a b -> c
  c f = Prelude.uncurry f . tT
  -- | Converts to a tuple.
  tT :: f a b -> (a, b)
  tT = c (uc (\ x -> x))
  -- | Converts from a tuple.
  dT :: (a, b) -> f a b
  dT = c (uc (\ x -> x))

instance Curriable (,) where
  tT x = x 
  dT x = x

instance Curriable ((,,) ()) where
  tT ((), a, b) = (a, b)
  dT (a, b) = ((), a, b)

instance
  Curriable p
    => Curriable (Flip p) where
  uc func = f (uc (func . (q :: Flip p a b -> p b a) ))
  c func = c (f func) . (q :: p b a -> Flip p a b)

-- | Nickname for 'List.group'.
g ::
  ( Eq a
  , Foldable f
  )
    => f a -> [NonEmpty [] a]
g = go . Foldable.toList
  where
    go [] = []
    go (x : xs) = (x :| ys) : g zs
      where
        (ys, zs) = List.span (== x) xs

-- | Nickname for `Prelude.mempty`.
k ::
  ( Monoid a
  )
    => a
k = mempty

-- | Nickname for 'Prelude.print'
pr ::
  ( Show a
  )
    => a -> IO ()
pr = Prelude.print

-- | Nickname for 'Prelude.iterate'.
-- Returns an 'IList' rather than a list.
x :: (a -> a) -> a -> IList a
x f a = f a :~ x f (f a)

-- | 'Prelude.flip' of 'x'.  Use as a shorthand for 'f' 'x'.
fx :: a -> (a -> a) -> IList a
fx = f x

-- | Two structures are zippable if they can be combined to for a third structure using a function.
-- If @f ~ g@ then:
--
-- prop> c (z (,)) (di x) = x
--
-- That is zipping with @(,)@ should reverse the process of unzipping.
-- Note that the opposite is /not/ required to be true.
--
-- It follows from this law that if @f ~ g@ then @f ~ h@.
-- That is zipping two structures of the same type should always result in a third structure of the same type.
class
  ( Zippable g f h
  , Functor f
  , Functor g
  )
    => Zippable f g h | f g -> h
  where
    {-# Minimal zi | z #-}
    -- | Equivalent to 'z' '($)' or 'f' 'zI'.
    -- Takes a list of functions and a list of arguments and applies the functions pairwise to get a new list.
    -- Compare with `(<*>)`.
    zi ::f (a -> b) -> g a -> h b
    zi = z ($)
    -- | More general version of 'Prelude.zipWith'.
    -- Compare with `l2`.
    z :: (a -> b -> c) -> f a -> g b -> h c
    z f a b = zi (f . a) b 

-- | 'z' as 'Applicative.liftA2'.
instance Zippable IO IO IO where
  z = Applicative.liftA2

-- | 'z' as 'Applicative.liftA2'.
instance Zippable ((->) a) ((->) a) ((->) a) where
  z :: (b -> c -> d) -> (a -> b) -> (a -> c) -> a -> d
  z q f g a = q (f a) (g a) 

instance Zippable [] [] [] where
  z = Prelude.zipWith

instance Zippable Maybe Maybe Maybe where
  z f N     _     = N
  z f _     N     = N
  z f (J x) (J y) = J (f x y)

instance Zippable Maybe [] Maybe where
  z f N     _        = N
  z f (J x) []       = N
  z f (J x) (y : ys) = J (f x y)

instance Zippable [] Maybe Maybe where
  z f _        N     = N
  z f []       (J y) = N
  z f (x : xs) (J y) = J (f x y)

instance
  ( Zippable f g h
  )
    => Zippable (NonEmpty f) (NonEmpty g) (NonEmpty h) where
  z f ~(x :| xs) ~(y :| ys) = f x y :| z f xs ys

instance
  ( Zippable f [] []
  )
    => Zippable (NonEmpty f) [] [] where
  z f ~(x :| xs) []       = []
  z f ~(x :| xs) (y : ys) = f x y : z f xs ys

instance
  ( Zippable [] g []
  )
    => Zippable [] (NonEmpty g) [] where
  z f []       ~(y :| ys) = []
  z f (x : xs) ~(y :| ys) = f x y : z f xs ys

instance
  ( Functor g
  )
    => Zippable (NonEmpty g) Maybe Maybe where
  z f ~(x :| xs) N     = N
  z f ~(x :| xs) (J y) = J (f x y)

instance
  ( Functor g
  )
    => Zippable Maybe (NonEmpty g) Maybe where
  z f N     ~(y :| ys) = N
  z f (J x) ~(y :| ys) = J (f x y)

-- | 'zd' zips with a function using a default value if it cannot source one of the values.
-- As an example
--
-- >>> zd (+) 0 0 [1,2,3] [4,5]
-- [5,7,3]
class DefaultZippable f g h | f g -> h where
  zd :: (a -> b -> c) -> a -> b -> f a -> g b -> h c

-- | 'zd' as 'Applicative.liftA2'.
instance DefaultZippable IO IO IO where
  zd f a b = Applicative.liftA2 f

-- | 'zd' as 'Applicative.liftA2'.
instance DefaultZippable ((->) a) ((->) a) ((->) a) where
  zd f a b = Applicative.liftA2 f

instance DefaultZippable ((->) a) Maybe ((->) a) where
  zd r _ b f N     x = r (f x) b 
  zd r _ _ f (J b) x = r (f x) b

instance DefaultZippable Maybe ((->) a) ((->) a) where
  zd r a b xs ys = zd (f r) b a ys xs

instance DefaultZippable [] [] [] where
  zd f a b []     []     = []
  zd f a b []     (y:ys) = f a y : zd f a b [] ys
  zd f a b (x:xs) []     = f x b : zd f a b xs []
  zd f a b (x:xs) (y:ys) = f x y : zd f a b xs ys

instance DefaultZippable Maybe Maybe Maybe where
  zd f a b N     N     = N
  zd f a b N     (J y) = J (f a y)
  zd f a b (J x) N     = J (f x b)
  zd f a b (J x) (J y) = J (f x y)

instance DefaultZippable Maybe [] [] where
  zd f a b N     ys = zd f a b []  ys
  zd f a b (J x) ys = zd f a b [x] ys

instance DefaultZippable [] Maybe [] where
  zd r a b xs ys = zd (f r) b a ys xs

instance
  ( DefaultZippable f g h
  )
    => DefaultZippable (NonEmpty f) (NonEmpty g) (NonEmpty h) where
  zd f a b ~(x :| xs) ~(y :| ys) = f x y :| zd f a b xs ys

instance
  ( DefaultZippable f [] h
  )
    => DefaultZippable (NonEmpty f) [] (NonEmpty h) where
  zd f a b ~(x :| xs) []       = f x b :| zd f a b xs []
  zd f a b ~(x :| xs) (y : ys) = f x y :| zd f a b xs ys

instance
  ( DefaultZippable f [] h
  )
    => DefaultZippable [] (NonEmpty f) (NonEmpty h) where
  zd r a b xs ys = zd (f r) b a ys xs

instance DefaultZippable (NonEmpty g) Maybe Identity where
  zd f a b ~(x :| xs) N     = I (f x b)
  zd f a b ~(x :| xs) (J y) = I (f x y)

instance DefaultZippable Maybe (NonEmpty g) Identity where
  zd r a b xs ys = zd (f r) b a ys xs

-- | More general version of 'Prelude.zip'.
-- Equivalent to @'z'(,)@.
z' ::
  ( Zippable f g h
  )
    => f a -> g b -> h (a, b)
z' = z (,)

-- | Equivalent to 'z' '(|>)' or 'f' 'zi'.
-- Takes a list of arguments and a list of functions and applies the functions pairwise to get a new list.
zI ::
  ( Zippable f g h
  )
    => f a -> g (a -> b) -> h b
zI = z (|>)

-- | Performs a zip ignoring the elements of the second argument.
-- Equivalent to @'z' 'p'@.
zp ::
  ( Zippable f g h
  )
    => f a -> g b -> h a
zp = z p

-- | Performs a zip ignoring the elements of the first argument.
-- Equivalent to @'z' 'fp'@.
zP ::
  ( Zippable f g h
  )
    => f a -> g b -> h b
zP = z (f p)

-- | Performs a zip using `(Prelude.<>)`.
-- Equivalent to @'z'('<>')@.
zs ::
  ( Zippable f g h
  , Semigroup a
  )
    => f a -> g a -> h a
zs = z (<>)

-- | Performs a zip using `(Prelude.<>)` and defaulting with the identity.
-- This means it will combine elements with `(Prelude.<>)` if both are present and use the present one if only one is present.
-- Equivalent to @'zd'('<>')'k' k@
zm ::
  ( DefaultZippable f g h
  , Monoid a
  )
    => f a -> g a -> h a
zm = zd (<>) mempty mempty
