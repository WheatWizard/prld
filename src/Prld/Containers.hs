{-# Language BangPatterns #-}
{-# Language FlexibleContexts #-}
{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
{-# Language QuantifiedConstraints #-}
{-# Language MultiParamTypeClasses #-}
{-# Language FunctionalDependencies #-}
{-# Language UndecidableSuperClasses #-}
module Prld.Containers
  ( -- * Classes
    Container
    ( ay
    , a
    , e
    , nl
    )
  , nA, na, ne, fe
  , aY, aN, e'
  , Filtrable
    ( fl
    , fN
    )
  , Empty
    ( em
    )
  , Concatable
    ( (++)
    )
  , Append
    ( (|:)
    )
  , Reversable
    ( r
    )
  , Consable
    ( (\:)
    )
  , Droppable
    ( d
    , d'
    )
  , Takeable
    ( t
    , t'
    )
  , Headed
    ( h
    )
  -- * Types
  , Maybe
    ( N
    , J
    )
  , Either
    ( R
    , L
    )
  , IList
    ( (:~)
    )
  , RIList
    ( (:*)
    )
  , NonEmpty
    ( (:|)
    )
  , BiNonEmpty
    ( (:||)
    )
  , tbp, fbp, lbp
  , Identity
    ( I
    )
  , Vacuous
    ( V
    )
  )
  where

import qualified Control.Applicative as Applicative
import qualified Control.Monad as Monad

import qualified Prelude
import Prelude
  ( Foldable
    ( foldMap
    )
  , Eq
    ( (==)
    )
  , Ord
  , Show
  , Read
  , Monoid
    ( mempty
    )
  , Num
    ( (+)
    , (-)
    )
  , Bool
    ( True
    , False
    )
  , (<)
  , Ordering
    ( EQ
    , LT
    , GT
    )
  )

import Prld.Monad
import Prld.Bool
import Prld.Coerce
import Prld.Number.Semigroup
import Prld.Number.Monoid
import Prld.Number.Group
import Prld.Number.Ring

($) = Prelude.id

-- | A replacement for 'Prelude.Maybe' that uses shorter constructor names.
-- A container with either 1 or fewer elements.
data Maybe a
  = N
  | J a
  deriving
    ( Show
    , Read
    , Eq
    , Ord
    )

instance Functor Maybe where
  m _ N     = N
  m f (J x) = J (f x)

instance Eq1 Maybe where
  lE eq N     N     = True
  lE eq (J x) (J y) = eq x y
  lE eq _     _     = False

instance Ord1 Maybe where
  lC cmp N     N     = EQ
  lC cmp (J x) (J y) = cmp x y
  lC cmp N     (J y) = LT
  lC cmp (J x) N     = GT

instance Pure Maybe where
  p = J

instance Foldable Maybe where
  foldMap f N     = mempty
  foldMap f (J a) = f a

-- | A replacement for 'Prelude.Either' that uses shorter constructor names.
data Either a b
  = L a
  | R b
  deriving
    ( Show
    , Read
    , Eq
    , Ord
    )

instance Functor (Either a) where
  m _ (L x) = L x
  m f (R x) = R (f x)

instance Functor (Flip Either b) where
  m f (F (L x)) = F (L (f x))
  m _ (F (R x)) = F (R x)

instance Eq a => Eq1 (Either a) where
  lE eq (L x) (L y) = x == y
  lE eq (R x) (R y) = eq x y
  lE eq _     _     = False

instance Ord a => Ord1 (Either a) where
  lC cmp (L x) (L y) = Prelude.compare x y
  lC cmp (R x) (R y) = cmp x y
  lC cmp (L x) (R y) = LT
  lC cmp (R x) (L y) = GT 

instance Pure (Either a) where
  p = R

-- | A list with infinite members.
-- It has a start but no end.
data IList a
  = a :~ IList a
  deriving
    ( Show
    , Eq
    , Ord
    )

instance Functor IList where
  m f (a :~ b) = f a :~ m f b

-- | Some functions may not halt
instance Foldable IList where
  -- Will not halt
  foldMap f ~(x :~ xs) = f x Prelude.<> foldMap f xs
  -- Will only halt if some element passes
  -- Use @e@ from @Container@ instead
  elem a ~(x :~ xs)
    | a Prelude.== x    = True
    | Prelude.otherwise = Prelude.elem a xs

instance Group1 IList where
  ln = m

instance Rng1 IList

instance Pure IList where
  p x = x :~ p x 

-- | A list with infinite members.
-- It has an end but no end.
-- Structurally identical to `IList` but with the direction reversed.
data RIList a
  = a :* RIList a
  deriving
    ( Show
    , Eq
    )

instance Functor RIList where
  m f (a :* b) = f a :* m f b

{-
instance Group1 RIList where
  ln = m

instance Rng1 RIList
-}

instance Pure RIList where
  p x = x :* p x

-- | Increases the possible sizes of a container by 1.
-- For example @NonEmpty []@ is a list with at least 1 element,
-- @NonEmpty Maybe@ has either 1 or 2 elements.
--
-- This has the effect of making sure that @NonEmpty p@ can never be empty, even if @p@ can.
data NonEmpty f a
  = a :| f a
  deriving
    ( Show
    , Read
    , Eq
    , Ord
    )

instance Functor f => Functor (NonEmpty f) where
  m f (a :| b) = f a :| m f b

instance Eq1 f => Eq1 (NonEmpty f) where
  lE eq (x :| xs) (y :| ys) = eq x y && lE eq xs ys

instance Ord1 f => Ord1 (NonEmpty f) where
  lC cmp (x :| xs) (y :| ys) =
    case
      cmp x y
    of
      EQ ->
        lC cmp xs ys
      other -> other

instance Pure (NonEmpty []) where
  p = (:| [])

instance Pure (NonEmpty Maybe) where
  p = (:| N)

instance Pure (NonEmpty Vacuous) where
  p = (:| V)

instance Pure (NonEmpty IList) where
  p = (:|) <*> p

instance Pure (NonEmpty RIList) where
  p = (:|) <*> p

instance Pure (NonEmpty Identity) where
  p = (:|) <*> p

instance Pure (NonEmpty (Either a)) where
  p = (:|) <*> p

instance
  ( Pure (NonEmpty a)
  )
    => Pure (NonEmpty (NonEmpty a)) where
  p = (:|) <*> p

-- | Increaseas the possible sizes of a bicontainer by 1.
-- For example @BiNonEmpty Map@ is a map with at least one key value pair.
--
-- Equivalent to @'Biproduct' (,)@.
data BiNonEmpty p a b
  = (a, b) :|| p a b
  deriving
    ( Show
    , Read
    , Eq
    , Ord
    )

-- | Converts a 'BiNonEmpty' to a @'Biproduct' (,)@.
tbp ::
  BiNonEmpty p a b -> Biproduct (,) p a b
tbp (xy :|| xys) = PR xy xys

-- | Converts a @'Biproduct' (,)@ to a 'BiNonEmpty'.
fbp ::
  Biproduct (,) p a b -> BiNonEmpty p a b
fbp (PR xy xys) = xy :|| xys

-- | Converts a function on @'Biproduct' (,)@s to a function on 'BiNonEmpty's.
lbp ::
  (Biproduct (,) p a b -> Biproduct (,) q c d) -> BiNonEmpty p a b -> BiNonEmpty q c d
lbp f = fbp . f . tbp

instance Functor (p a) => Functor (BiNonEmpty p a) where
  m f = lbp (m f)

instance Functor (Flip p a) => Functor (Flip (BiNonEmpty p) a) where
  m f (F x) = F (lbp (m2 f) x)

-- TODO Bipure instances for BiNonEmpty

-- | Version of Identity
newtype Identity a
  = I a
  deriving
    ( Show
    , Read
    , Eq
    , Ord
    )

instance Functor Identity where
  m f (I a) = I (f a)

instance Eq1 Identity where
  lE eq (I x) (I y) = eq x y

instance Ord1 Identity where
  lC cmp (I x) (I y) = cmp x y

instance Group1 Identity where
  ln = m

instance Rng1 Identity

instance Pure Identity where
  p = I

-- | A container with no elements.
data Vacuous a
  = V
  deriving
    ( Show
    , Read
    , Eq
    , Ord
    )

instance Functor Vacuous where
  m f _ = V

instance Contravariant Vacuous where
  contramap f _ = V

instance Eq1 Vacuous where
  lE eq _ _ = True

instance Ord1 Vacuous where
  lC cmp _ _ = EQ

instance Group1 Vacuous where
  ln = m

instance Rng1 Vacuous

instance Pure Vacuous where
  p = p V

instance Apply Vacuous f Vacuous where
  _ <*> x = V

-- | A container is a functor for which we can check properties on its contents.
--
-- Implementation of 'e', 'a', 'ay' and 'nl' should match the default.
-- That is:
--
-- prop> e x = ay (== x)
-- prop> ay f = n . a (n . f)
-- prop> a f = n . ay (n . f)
-- prop> nl = ay (p True)
--
-- The additional law should be satisfied:
--
-- prop> e thing container = e (func thing) (m func container)
--
class Functor f => Container f where
  {-# Minimal a | ay | e #-}
  -- | Generalized 'Prelude.any'.
  -- Returns @True@ (or equivalent) is there is an element satisfying the predicate.
  -- Returns 
  ay :: Boolish b => (a -> b) -> f a -> b
  ay f = n . a (n . f)
  -- | Generalized 'Prelude.all'.
  -- Returns @True@ (or equivalent) if every element in a container satisfies the predicate.
  a :: Boolish b => (a -> b) -> f a -> b
  a f = dB . n . e bt . m f
  -- | Generalized 'Prelude.elem'.
  -- Checks if an element is present in a container
  e ::
    ( Eq a
    )
      => a -> f a -> Bool
  e x = ay (Prelude.== x)
  -- | Determines whether a container is empty.
  -- Replaces `Prelude.null`
  nl :: f a -> Bool
  nl = ay (p True)

instance Container Vacuous where
  ay p ~V = bt

instance Container Identity where
  ay p (I a) = p a

  nl _ = True

instance Container Maybe where
  ay p N     = bt
  ay p (J x) = p x

instance Container (Either a) where
  ay f (L x) = bt
  ay f (R x) = f x

instance Container [] where
  ay f []       = bt
  ay f (x : xs)
    | tB (f x)  = tp
  ay f (x : xs) = ay f xs

instance Container IList where
  ay f (x :~ xs)
    | tB (f x)   = tp
  ay f (x :~ xs) = ay f xs

  nl _ = False

instance Container RIList where
  ay f (x :* xs)
    | tB (f x)   = tp
  ay f (x :* xs) = ay f xs

  nl _ = False

-- Useful for compositions
instance Container ((,) a) where
  ay f (x1, x2) = f x2 

  a  f (x1, x2) = f x2

  e  y (x1, x2) = y == x2

  nl _ = False

-- Useful for compositions
instance Container ((,,) a b) where
  ay f (x1, x2, x3) = f x3 

  a  f (x1, x2, x3) = f x3

  e  y (x1, x2, x3) = y == x3

  nl _ = False

instance Container f => Container (NonEmpty f) where
  ay f (x :| xs)
    | tB (f x)   = tp
  ay f (x :| xs) = ay f xs

  a f (x :| xs)
    | tB (f x)  = a f xs
  a f (x :| xs) = bt

  e y (x :| xs)
    | y == x    = True
  e y (x :| xs) = e y xs

  nl _ = False

instance
  ( Container f
  , Container g
  )
    => Container (Compose f g) where
  ay f (C x) = ay (ay f) x

  a  f (C x) = a  (a  f) x

  e  y (C x) = ay (e  y) x

  nl   (C x) = a  nl     x

instance
  ( Container f
  , Container g
  )
    => Container (Product f g) where
  ay f (Pr x y) = ay f x || ay f y

  a  f (Pr x y) = a  f x && a  f y

  e  f (Pr x y) = e  f x || e  f y

  nl   (Pr x y) = nl   x && nl   y

instance
  ( Container (f a)
  , Container (g a)
  )
    => Container (Biproduct f g a) where
  ay f (PR x y) = ay f x || ay f y

  a  f (PR x y) = a  f x && a  f y

  e  f (PR x y) = e  f x || e  f y

  nl   (PR x y) = nl   x && nl   y

instance
  ( Container f
  , Container g
  )
    => Container (Sum f g) where
  ay f (Il x) = ay f x
  ay g (Ir x) = ay g x

  a  f (Il x) = a  f x
  a  f (Ir x) = a  f x

  e  f (Il x) = e  f x
  e  f (Ir x) = e  f x

  nl   (Il x) = nl   x
  nl   (Ir x) = nl   x

instance
  ( Container (f a)
  , Container (g a)
  )
    => Container (Bisum f g a) where
  ay f (IL x) = ay f x
  ay g (IR x) = ay g x

  a  f (IL x) = a  f x
  a  f (IR x) = a  f x

  e  f (IL x) = e  f x
  e  f (IR x) = e  f x

  nl   (IL x) = nl   x
  nl   (IR x) = nl   x

instance
  ( Container f
  , Container (g a)
  )
    => Container (Bicompose f g a) where
  ay f (Bc x) = ay (ay f) x

  a  f (Bc x) = a  (a  f) x

  e  y (Bc x) = ay (e  y) x

  nl   (Bc x) = a  nl     x

instance
  ( Container f
  , Container (p (f a))
  )
    => Container (On p f a) where
  ay f (On x) = ay (ay f) x

  a  f (On x) = a  (a  f) x

  e  y (On x) = ay (e  y) x

  nl   (On x) = a  nl     x

instance
  ( Container (p a)
  )
    => Container (Flip (Flip p) a) where
  ay f (F (F x)) = ay f x

  a  f (F (F x)) = a  f x

  e  y (F (F x)) = e  y x

  nl   (F (F x)) = nl   x

instance
  ( forall a . Container (p a)
  , forall a . Container (Flip p a)
  )
    => Container (Join p) where
  ay f (W x) = ay f x || aY f x

  a  f (W x) = a  f x && aN f x

  e  y (W x) = e  y x || e' y x

  nl   (W x) = nl   x && nL   x

instance Container (Flip (,) a) where
  ay f (F (x1, x2)) = f x1

  a  f (F (x1, x2)) = f x1

  e  y (F (x1, x2)) = y == x1

  nl   _            = False

instance Container (Flip ((,,) a) b) where
  ay f (F (x1, x2, x3)) = f x2

  a  f (F (x1, x2, x3)) = f x2

  e  y (F (x1, x2, x3)) = y == x2

  nl   _                = False

instance Container (Flip Either a) where
  ay f (F (L x)) = f x
  ay f (F (R x)) = bt

instance
  ( Container f
  , Container (Flip g a)
  )
    => Container (Flip (Bicompose f g) a) where
  ay f (F (Bc x)) = ay (aY f) x

  a  f (F (Bc x)) = a  (aN f) x

  e  f (F (Bc x)) = ay (e' f) x

  nl   (F (Bc x)) = a  nL     x

instance
  ( Container f
  , Container (Flip p (f a))
  )
    => Container (Flip (On p f) a) where
  ay f (F (On x)) = aY (ay f) x

  a  f (F (On x)) = aN (a  f) x

  e  f (F (On x)) = aY (e  f) x

  nl   (F (On x)) = aN nl     x

instance
  ( Container (Flip p a)
  , Container (Flip q a)
  )
    => Container (Flip (Biproduct p q) a) where
  ay f (F (PR x y)) = aY f x || aY f y

  a  f (F (PR x y)) = aN f x && aN f y

  e  f (F (PR x y)) = e' f x || e' f y

  nl   (F (PR x y)) = nL   x && nL   y

instance
  ( Container (Flip f a)
  , Container (Flip g a)
  )
    => Container (Flip (Bisum f g) a) where
  ay f (F (IL x)) = aY f x
  ay g (F (IR x)) = aY g x

  a  f (F (IL x)) = aN f x
  a  f (F (IR x)) = aN f x

  e  f (F (IL x)) = e' f x
  e  f (F (IR x)) = e' f x

  nl   (F (IL x)) = nL x
  nl   (F (IR x)) = nL x

-- | The negation of 'a'.
-- Returns 'Prelude.True' iff some element does not satisfy the predicate.
na ::
  ( Container f
  , Boolish b
  )
    => (a -> b) -> f a -> b
na = (n .) . a

-- | The negation of 'ay'.
-- Returns 'Prelude.True' iff every element does not satisfy the predicate.
nA ::
  ( Container f
  , Boolish b
  )
    => (a -> b) -> f a -> b
nA = (n .) . ay

-- | The negation of 'e'.
-- Returns 'Prelude.True' iff no element is equal to the given element.
ne ::
  ( Container f
  , Eq a
  )
    => a -> f a -> Bool
ne = (n .) . e

-- | The flip of 'e'.
-- Checks whether the second argument is a member of the first argument
fe ::
  ( Container f
  , Eq a
  )
    => f a -> a -> Bool
fe xs x = e x xs

aY ::
  ( Container (Flip f c)
  , Boolish b
  )
    => (a -> b) -> f a c -> b
aY p = ay p . F

aN ::
  ( Container (Flip f c)
  , Boolish b
  )
    => (a -> b) -> f a c -> b
aN p = a p . F

e' ::
  ( Container (Flip f c)
  , Eq a
  )
    => a -> f a c -> Bool
e' x = e x . F

nL ::
  ( Container (Flip f c)
  )
    => f a c -> Bool
nL = nl . F


-- | Containers for which arbitrary elements can be removed.
--
-- Since structure can be lost this way the starting container may not be the same type as the result container.
--
-- Instances should follor these laws:
--
-- prop> a prop (fl prop cont) = True
-- prop> ay prop (fl prop cont) = ay prop cont
class (Container f, Container g) => Filtrable f g | f -> g where
  {-# Minimal fl | fN #-}
  -- | A positive filter.
  -- Keeps all elements that satisfy a predicate.
  fl :: (a -> Bool) -> f a -> g a
  fl = fN . (Prelude.not .)
  -- | A negative filter.
  -- Removes all elements that satisfy a predicate.
  fN :: (a -> Bool) -> f a -> g a
  fN = fl . (Prelude.not .)

instance Filtrable Vacuous Vacuous where
  fl p ~V = V

instance Filtrable [] [] where
  fl p [] = []
  fl p (a : b)
    | p a  = a : fl p b
    | True = fl p b

instance Filtrable IList [] where
  fl p (x :~ xs)
    | p x  = x : fl p xs
    | True = fl p xs

instance Filtrable Maybe Maybe where
  fl p N = N
  fl p (J x)
    | p x  = J x
    | True = N

instance Filtrable Identity Maybe where
  fl p (I x)
    | p x  = J x
    | True = N

instance
  ( Filtrable f g
  , Consable g
  )
    => Filtrable (NonEmpty f) g where
  fl p (x :| xs)
    | p x  = x \: fl p xs
    | True = fl p xs

instance
  ( Filtrable f h
  , Filtrable g i
  )
    => Filtrable (Sum f g) (Sum h i) where
  fl p (Il xs) = Il (fl p xs)
  fl p (Ir xs) = Ir (fl p xs)

instance
  ( Filtrable f h
  , Filtrable g i
  )
    => Filtrable (Product f g) (Product h i) where
  fl p (Pr xs ys) = Pr (fl p xs) (fl p ys)

-- | For containers that can be empty.
--
-- The following law should be satisfied:
--
-- prop> ay f em = False
class Container f => Empty f where
  em :: f a

instance Empty [] where
  em = []

instance Empty Maybe where
  em = N

instance Empty Vacuous where
  em = V

instance
  ( Empty f
  , Container g
  )
    => Empty (Compose f g) where
  em = C em

instance
  ( Empty f
  , Container (p a)
  )
    => Empty (Bicompose f p a) where
  em = Bc em

-- | For structures that can be concatenated.
--
-- The following laws should be held:
--
-- prop> (f . a) ++ (f . b) = f . (a ++ b)
--
-- prop> ay f a || ay f b = ay f (a ++ b) 
class
  ( Container f
  , Container g
  , Container h
  )
    => Concatable f g h | f g -> h where
  -- | Generalization of '(Prelude.++)'
  (++) :: f a -> g a -> h a  
  infixr 5 ++

instance
  ( Concatable f IList h
  , Reversable f g
  , Reversable h i
  )
    => Concatable RIList g i where
  a ++ b = r (r b ++ r a)

-- Conflicts with instances where g is not Consable
-- e.g. g ~ Maybe
--
-- Instead we have to mirror instances of Consable using this format
{-
instance
  ( Consable g
  )
    => Concatable [] g g where
  []       ++ ys = ys
  (x : xs) ++ ys = x \: (xs ++ ys)
-}

instance Concatable [] [] [] where
  []       ++ ys = ys
  (x : xs) ++ ys = x \: (xs ++ ys)

instance Concatable [] IList IList where
  []       ++ ys = ys
  (x : xs) ++ ys = x \: (xs ++ ys)

instance
  ( Consable f
  )
    => Concatable [] (NonEmpty f) (NonEmpty f) where
  []       ++ ys = ys
  (x : xs) ++ ys = x \: (xs ++ ys)

instance Concatable Maybe Maybe [] where
  N   ++ N   = []
  J x ++ N   = [x]
  N   ++ J x = [x]
  J x ++ J y = [x, y]

instance Concatable [] Maybe [] where
  xs ++ N   = xs
  xs ++ J y = xs ++ [y]

instance
  ( Concatable f [] i
  , Concatable (NonEmpty i) g (NonEmpty h)
  )
    => Concatable (NonEmpty f) (NonEmpty g) (NonEmpty h) where
  (x :| xs) ++ (y :| ys) = (x :| xs ++ [y]) ++ ys

-- Valid instance
-- However when g ~ IList we don't want the result to be NonEmpty IList
-- Since that type is rather meaningless.
-- Instead we enumerate each instance separately
{-
instance
  ( Concatable f g h
  )
    => Concatable (NonEmpty f) g (NonEmpty h) where
  (x :| xs) ++ ys = x :| xs ++ ys
-}

instance
  ( Concatable f [] h
  )
    => Concatable (NonEmpty f) [] (NonEmpty h) where
  (x :| xs) ++ ys = x :| xs ++ ys

instance
  ( Concatable (NonEmpty f) [] (NonEmpty h)
  , Container f -- Can be deduced
  , Container h -- Can be deduced
  )
    => Concatable (NonEmpty f) Maybe (NonEmpty h) where
  xs ++ N   = xs ++ []
  xs ++ J y = xs ++ [y]

instance
  ( Concatable f IList IList
  )
    => Concatable (NonEmpty f) IList IList where
  (x :| xs) ++ ys = x :~ (xs ++ ys)

instance
  ( Consable f
  )
    => Concatable Maybe (NonEmpty f) (NonEmpty f) where
  N   ++ ys = ys
  J a ++ ys = a \: ys

instance Concatable Maybe [] [] where
  N   ++ ys = ys
  J x ++ ys = x : ys

instance Concatable Maybe IList IList where
  N     ++ ys = ys
  (J x) ++ ys = x :~ ys

instance
  ( Container f
  )
    => Concatable Identity f (NonEmpty f) where
  (I x) ++ ys = x :| ys

-- | A container that can have an element appended to make a nonempty version.
class Container f => Append f where
  (|:) :: a -> f a -> NonEmpty f a

instance Append [] where
  x |: []       = x :| []
  x |: (y : ys) = y :| (ys ++ [x])

instance Append Maybe where
  x |: N     = x :| N
  x |: (J y) = y :| (J x)

instance Append Identity where
  x |: (I y) = y :| (I x)

instance Append IList where
  _ |: (y :~ ys) = y :| ys

instance Append Vacuous where
  x |: ~V = x :| V

instance
  ( Append f
  )
    => Append (NonEmpty f) where
  x |: (y :| ys) = y :| (x |: ys)

-- | A container which can have its order reversed.
--
-- Should satisfy the laws
--
-- prop> ay f x = ay f (r x)
-- prop> a f x = a f (r x)
-- prop> r (r x) = x
class
  ( Container f
  , Container g
  , Reversable g f
  )
    => Reversable f g
    | f -> g
    , g -> f
    where
  -- | Generalization of 'Prelude.reverse'.
  --
  -- Reverses the order of reversable container.
  r :: f a -> g a

instance Reversable [] [] where
  r = Prelude.reverse

instance Reversable Maybe Maybe where
  r x = x

instance Reversable Vacuous Vacuous where
  r _ = V

instance Reversable Identity Identity where
  r x = x

instance Reversable IList RIList where
  r (x :~ xs) = x :* r xs 

instance Reversable RIList IList where
  r (x :* xs) = x :~ r xs

instance Reversable ((,) a) ((,) a) where
  r (x1, x2) = (x1, x2)

instance Reversable ((,,) a b) ((,,) a b) where
  r (x1, x2, x3) = (x1, x2, x3)

instance Reversable (Flip (,) a) (Flip (,) a) where
  r (F (x1, x2)) = F (x1, x2)

instance Reversable (Join (,)) (Join (,)) where
  r (W (x1, x2)) = W (x2, x1)

instance Reversable (Join ((,,) a)) (Join ((,,) a)) where
  r (W (x1, x2, x3)) = W (x1, x3, x2)

-- TODO Join . Flip

{-
instance Reversable (Join (Join (,,))) (Join (Join (,,))) where
  r (W (W (x1, x2, x3))) = (W . W) (x3, x2, x1)
-}

instance
  ( Reversable (p a) (q b)
  )
    => Reversable (Flip (Flip p) a) (Flip (Flip q) b) where
  r = (lF . lF) r

instance
  ( Reversable f g
  , Reversable h i
  )
    => Reversable (Sum f h) (Sum g i) where
  r (Il x) = Il (r x)
  r (Ir x) = Ir (r x)

instance
  ( Append f
  , Append g
  , Reversable f g
  )
    => Reversable (NonEmpty f) (NonEmpty g) where
  r (h :| t) = h |: r t

instance
  ( Reversable f h
  , Reversable g i
  )
    => Reversable (Compose f g) (Compose h i) where
  r (C x) = C (r (m r x))

instance
  ( Reversable f h
  , Reversable (p a) (q b)
  )
    => Reversable (Bicompose f p a) (Bicompose h q b) where
  r (Bc x) = Bc (r (m r x))

instance
  ( Reversable f h
  , Reversable (Flip p a) (Flip q b)
  )
    => Reversable (Flip (Bicompose f p) a) (Flip (Bicompose h q) b) where
  r (F (Bc x)) = (F . Bc . r . m (q . r . F)) x

instance
  ( Reversable f h
  , Reversable (p (f a)) (q (h b))
  )
    => Reversable (On p f a) (On q h b) where
  r (On x) = On (r (m r x))

instance
  ( Reversable f h
  , Reversable (Flip p (f a)) (Flip q (h b))
  )
    => Reversable (Flip (On p f) a) (Flip (On q h) b) where
  r (F (On x)) = (F . On . q . m r . r . F) x

-- | For structures that have a cons operation.
--
-- Instances should fullfill the laws:
--
-- prop> f . (a \: b) = f a \: (f . b)
--
-- prop> e a (a \: b) = True
class Container f => Consable f where
  (\:) :: a -> f a -> f a

instance Consable [] where
  (\:) = (:)

instance Consable IList where
  (\:) = (:~) 

instance Consable f => Consable (NonEmpty f) where
  a \: (x :| xs) = a :| (x \: xs)

-- | For structures that can drop an amount.
--
-- @f@ and @g@ may be different types.
-- For example if @f ~ 'NonEmpty' []@, dropping elements cannot guarentee that the result will be non-empty so @g ~ []@.
--
-- If @f@ and @g@ are 'Functor's it should obey the law:
--
-- prop> d n (f . x) = f . d n x
class Droppable f g | f -> g where
  -- | Generalization of 'Prelude.drop'
  d ::
    ( Ord a
    , Num a
    )
      => a -> f b -> g b
  d = Prelude.snd ... d' 
  -- | A drop that also returns the amount unconsumed.
  d' ::
    ( Ord a
    , Num a
    )
      => a -> f b -> (a, g b)

instance Droppable [] [] where
  -- We implement d since it is lazier on the list than d'
  -- d' needs to know whether it is empty in order to know the amount left
  -- d doesn't care
  d n xs
    | n < 1 = xs
  d n []       = []
  d n (x : xs) = d (n - 1) xs
  d' n [] = (n, [])
  d' n (x : xs)
    | n < 1
      = (n, x : xs)
    | Prelude.otherwise
      = d' (n - 1) xs

instance Droppable Maybe Maybe where
  -- We implement d since it is lazier on the Maybe than d'
  -- d' needs to know whether it is a Just or a Nothing in order to know the amount left
  -- d doesn't care
  d n x
    | n < 1
      = x
    | Prelude.otherwise
      = N
  d' n N = (n, N)
  d' n (J x)
    | n < 1
      = (n, J x)
    | Prelude.otherwise
      = (n - 1, N)

instance Droppable Identity Maybe where
  d' n (I x)
    | n < 1
      = (n, J x)
    | Prelude.otherwise
      = (n - 1, N)

instance Droppable Vacuous Vacuous where
  d' n ~V = (n, V)

instance
  ( Droppable f g
  , Consable g
  )
    => Droppable (NonEmpty f) g where
  d' n (x :| xs)
    | n < 1
    , (l, ys) <- d' (n - 1) xs
      = (l + 1, x \: ys)
    | Prelude.otherwise
      = d' (n - 1) xs

instance Droppable IList IList where
  d' n (x :~ xs)
    | n < 1
      = (n, x :~ xs)
    | Prelude.otherwise
      = d' (n - 1) xs

-- | For structures that can take an amount.
--
-- @f@ and @g@ may be different types.
-- For example if @f ~ 'IList'@, taking a finite number of elements means that the result will no longer be infinite so @g ~ []@.
--
-- It should obey the law:
--
-- prop> t n (f . x) = f . t n x
class
  ( Container f
  , Container g
  )
    => Takeable f g
    | f -> g
    where
  -- | Generalization of 'Prelude.take'.
  t ::
    ( Num a
    , Ord a
    )
      => a -> f b -> g b
  t = (Prelude.snd .) . t'
  -- | A take that also returns the amount unconsumed.
  t' ::
    ( Num a
    , Ord a
    )
      => a -> f b -> (a, g b)

instance Takeable [] [] where
  t' n [] = (n, [])
  t' n (x : xs)
    | n < 1 =
      (n, [])
    | (left, taken) <- t' (n - 1) xs =
      (left, x : taken)

instance
  ( Takeable f g
  , Empty g
  , Consable g
  )
    => Takeable (NonEmpty f) g where
  t' n (x :| xs)
    | n < 1 =
      (n, em)
    | (left, taken) <- t' (n - 1) xs =
      (left, x \: taken)

instance Takeable Maybe Maybe where
  t' n N = (n, N)
  t' n (J x)
    | n < 1             = (n, N)
    | Prelude.otherwise = (n - 1, J x)

instance Takeable Identity Maybe where
  t' n (I x)
    | n < 1             = (n, N)
    | Prelude.otherwise = (n - 1, J x)

instance Takeable IList [] where
  t' n (x :~ xs)
    | n < 1 =
      (n, [])
    | (left, taken) <- t' (n - 1) xs =
      (left, x : taken)


class Headed f g | f -> g where
  h :: f -> g

instance Headed (NonEmpty f a) a where
  h (x :| xs) = x

instance Headed (IList a) a where
  h (x :~ xs) = x

instance Headed (Identity a) a where
  h (I a) = a

instance Headed [a] (Maybe a) where
  h [] = N
  h (a : b) = J a

instance Headed (Maybe a) (Maybe a) where
  h xs = xs

-- Apply instances


instance Apply IList IList IList where
  a <*> b = a >>= (. b)

instance Apply Maybe Maybe Maybe where
  N     <*> _     = N
  _     <*> N     = N
  (J f) <*> (J x) = J (f x)

instance
  ( Functor f
  )
    => Apply Identity f f
  where
    (I f) <*> x = m f x

instance Apply Maybe [] [] where
  N     <*> _  = []
  (J f) <*> xs = f . xs

instance Apply [] Maybe [] where
  _  <*> N     = []
  fs <*> (J x) = ($ x) . fs

instance Apply (NonEmpty []) Maybe [] where
  _          <*> N     = []
  ~(f :| fs) <*> (J x) = ($ x) . (f : fs)

instance Apply Maybe (NonEmpty []) [] where
  N     <*> _          = []
  (J f) <*> ~(x :| xs) = f . (x : xs)

instance Apply Maybe (NonEmpty Maybe) [] where
  N     <*> _          = []
  (J f) <*> (x :| N)   = [f x]
  (J f) <*> (x :| J y) = [f x, f y]

instance Apply [] (NonEmpty []) [] where
  fs <*> ~(x :| xs) = fs <*> (x : xs)

instance Apply (NonEmpty []) [] [] where
  ~(f :| fs) <*> xs = (f : fs) <*> xs

instance Apply IList (NonEmpty []) IList where
  a <*> b = a >>= (. b)

instance Apply (NonEmpty []) IList IList where
  a <*> b = a >>= (. b)

instance
  ( Concatable (NonEmpty g) i h
  , Apply f (NonEmpty g) i
  , Functor g
  )
    => Apply (NonEmpty f) (NonEmpty g) h where
  (f :| fs) <*> xs = (f . xs) ++ (fs <*> xs)

instance
  ( Apply p q r
  , Apply s t u
  )
    => Apply (Compose p s) (Compose q t) (Compose r u) where
  C f <*> C x = C (l2 (<*>) f x)

  l2 func (C x) (C y) = C (l2 (l2 func) x y)

instance
  ( Apply p q r
  , Apply (s a) (t b) (u c)
  )
    => Apply (Bicompose p s a) (Bicompose q t b) (Bicompose r u c) where
  Bc f <*> Bc x = Bc (l2 (<*>) f x)

  l2 func (Bc x) (Bc y) = Bc (l2 (l2 func) x y)

instance
  ( Apply f g h
  , Apply (p (f a)) (q (g b)) (r (h c))
  )
    => Apply (On p f a) (On q g b) (On r h c) where
  On f <*> On x = On (l2 (<*>) f x)

  l2 func (On x) (On y) = On (l2 (l2 func) x y)

instance
  ( Apply (p a) (q b) (r c)
  )
    => Apply (Flip (Flip p) a) (Flip (Flip q) b) (Flip (Flip r) c) where
  F (F x) <*> F (F y) = (F . F) (x <*> y)

  l2 func (F (F x)) (F (F y)) = (F . F) (l2 func x y)

instance
  ( Apply f g h
  , Apply (Flip p a) (q b) (r c)
  )
    => Apply (Flip (Bicompose f p) a) (Bicompose g q b) (Bicompose h r c) where
  F (Bc f) <*> Bc x = Bc (l2 (<*>) (m F f) x)

  l2 func (F (Bc x)) (Bc y) = Bc (l2 (l2 func) (m F x) y)

instance
  ( Apply f g h
  , Functor g
  , Apply (p a) (Flip q b) (r c)
  )
    => Apply (Bicompose f p a) (Flip (Bicompose g q) b) (Bicompose h r c) where
  Bc f <*> F (Bc x) = Bc (l2 (<*>) f (m F x))

  l2 func (Bc x) (F (Bc y)) = Bc (l2 (l2 func) x (m F y))

instance
  ( Apply f g h
  , Functor g
  , Apply (Flip p a) (Flip q b) (r c)
  )
    => Apply (Flip (Bicompose f p) a) (Flip (Bicompose g q) b) (Bicompose h r c) where
  F (Bc f) <*> F (Bc x) = Bc (l2 (<*>) (m F f) (m F x))

  l2 func (F (Bc x)) (F (Bc y)) = Bc (l2 (l2 func) (m F x) (m F y))

instance
  ( Apply f g h
  , Apply (Flip p (f a)) (q (g b)) (r (h c))
  )
    => Apply (Flip (On p f) a) (On q g b) (On r h c) where
  F (On f) <*> On x = On (l2 (<*>) (F f) x)

  l2 func (F (On x)) (On y) = On (l2 (l2 func) (F x) y)

instance
  ( Apply f g h
  , Apply (p (f a)) (Flip q (g b)) (r (h c))
  )
    => Apply (On p f a) (Flip (On q g) b) (On r h c) where
  On f <*> F (On x) = On (l2 (<*>) f (F x))

  l2 func (On x) (F (On y)) = On (l2 (l2 func) x (F y))

instance
  ( Apply f g h
  , Apply (Flip p (f a)) (Flip q (g b)) (r (h c))
  )
    => Apply (Flip (On p f) a) (Flip (On q g) b) (On r h c) where
  F (On f) <*> F (On x) = On (l2 (<*>) (F f) (F x))

  l2 func (F (On x)) (F (On y)) = On (l2 (l2 func) (F x) (F y))

-- Bind instances

instance
  ( Concatable (NonEmpty g) i h
  , Bind f (NonEmpty g) i
  , Functor g
  )
    => Bind (NonEmpty f) (NonEmpty g) h where
  ~(a :| as) >>= f = f a ++ (as >>= f)

instance Bind IList IList IList where
  j = go 1
    where
      go :: Prelude.Integer -> IList (IList a) -> IList a
      go n lss =
        let
          hss = t n lss
        in
          h . hss ++ go (n + 1) (d 1 . hss ++ d n lss)

instance Bind Maybe Maybe Maybe where
  j N = N
  j (J N) = N
  j (J (J x)) = J x

instance Bind [] Maybe [] where
  j []         = []
  j (N   : xs) =     j xs
  j (J x : xs) = x : j xs

instance Bind Maybe [] [] where
  j N     = []
  j (J x) = x

instance
  ( Bind f Maybe h
  , Apply (NonEmpty f) Maybe h
  , Consable h
  )
    => Bind (NonEmpty f) Maybe h where
  j (N   :| xs) =      j xs
  j (J x :| xs) = x \: j xs

instance Bind Maybe (NonEmpty []) [] where
  j N             = []
  j (J (x :| xs)) = x : xs

instance Bind Maybe (NonEmpty Maybe) [] where
  j N              = []
  j (J (x :| N))   = [x]
  j (J (x :| J y)) = [x, y]

instance Bind [] (NonEmpty []) [] where
  j [] = []
  j ((x :| xs) : xss) = x : xs ++ j xss

instance
  ( Bind f [] g
  , Concatable [] g h
  , Apply (NonEmpty f) [] h
  )
    => Bind (NonEmpty f) [] h where
  j ~(x :| xs) = x ++ j xs

instance Bind IList (NonEmpty []) IList where
  j ~(x :~ xs) = x ++ j xs

instance Bind (NonEmpty []) IList IList where
 j xs = h . xs ++ j (d 1 . xs) 

{-
instance Bind IList [] IList where
  j (x :~ xs) = x ++ j xs

instance Bind IList Maybe IList where
  j (x :~ xs) = x ++ j xs

instance Bind IList (NonEmpty []) IList where
  j (x :~ xs) = x ++ j xs
-}
