{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language QuantifiedConstraints #-}
module Prld.Traverse
  ( Traversable
    ( tr
    , sA
    )
  , StrongTraversable
    ( tr'
    , sA'
    )
  ) where

import qualified Prelude
import Prelude
  (
  )


import Prld.Containers
import Prld.Fold
import Prld.Monad

class Traversable t => StrongTraversable t where
  {-# Minimal tr' | sA' #-}
  tr' ::
    ( Apply f f f
    )
      => (a -> f b) -> t a -> f (t b)
  tr' f = sA' . m f 
  sA' ::
    ( Apply f f f
    )
      => t (f a) -> f (t a)
  sA' = tr' (\ x -> x)

class Foldable t => Traversable t where
  {-# Minimal tr | sA #-}
  tr ::
    ( Apply f f f
    , Pure f
    )
      => (a -> f b) -> t a -> f (t b)
  tr f = sA . m f 
  sA ::
    ( Apply f f f
    , Pure f
    )
      => t (f a) -> f (t a)
  sA = tr (\ x -> x)

instance Traversable [] where
  sA xs = fR (l2 (:)) (p []) xs

instance Traversable Maybe where
  sA N     = p N
  sA (J x) = m J x

instance Traversable (Either a) where
  sA (R x) = m R x
  sA ~(L y) = p (L y)

instance Traversable (Flip Either a) where
  sA (F (L x)) = m (F . L) x
  sA ~(F (R y)) = p (F (R y))

instance Traversable Identity where
  sA = sA'
  tr = tr'

instance StrongTraversable Identity where
  sA' (I x) = m I x

instance Traversable ((,) a) where
  sA = sA'
  tr = tr'

instance StrongTraversable ((,) a) where
  sA' (y, x) = m ((,) y) x

instance Traversable (Flip (,) a) where
  sA = sA'
  tr = tr'

instance StrongTraversable (Flip (,) a) where
  sA' (F (x, y)) = m (\ a -> F (a, y)) x

instance Traversable IList where
  sA = sA'
  tr = tr'

instance StrongTraversable IList where
  sA' ~(x :~ xs) = l2 (:~) x (sA' xs)

instance Traversable Vacuous where
  sA V = p V
  tr f V = p V

instance
  ( Traversable t
  )
    => Traversable (NonEmpty t) where
  sA ~(x :| xs) = l2 (:|) x (sA xs)
  tr f ~(x :| xs) = l2 (:|) (f x) (tr f xs)

instance
  ( StrongTraversable t
  )
    => StrongTraversable (NonEmpty t) where
  sA' ~(x :| xs) = l2 (:|) x (sA' xs)
  tr' f ~(x :| xs) = l2 (:|) (f x) (tr' f xs)

instance
  ( Traversable (t a)
  )
    => Traversable (Flip (Flip t) a) where
  sA (F (F xs)) = m (F . F) (sA xs)
  tr f (F (F xs)) = m (F . F) (tr f xs)

instance
  ( StrongTraversable (t a)
  )
    => StrongTraversable (Flip (Flip t) a) where
  sA' (F (F xs)) = m (F . F) (sA' xs)
  tr' f (F (F xs)) = m (F . F) (tr' f xs)

instance Traversable (Join (,)) where
  sA = sA'
  tr = tr'

instance StrongTraversable (Join (,)) where
  sA' (W (x, y)) = l2 (W ... (,)) x y

instance Traversable (Join Either) where
  sA = sA'
  tr = tr'

instance StrongTraversable (Join Either) where
  sA' (W (L x)) = m (W . L) x
  sA' (W (R x)) = m (W . R) x

-- | Performs a flip under a Join.
--
-- Useful for the next instance.  SHOULD NOT BE EXPORTED!
reFlip :: Join p a -> Join (Flip p) a
reFlip = lW F

instance
  ( Traversable (Join p)
  , forall a . Foldable (p a)
  , forall a . Foldable (Flip p a)
  )
    => Traversable (Join (Flip p)) where
  sA (W (F x)) =
    (m reFlip . sA . W) x

  tr func (W (F x)) =
    (m reFlip . tr func . W) x

instance
  ( StrongTraversable (Join p)
  , forall a . Foldable (p a)
  , forall a . Foldable (Flip p a)
  )
    => StrongTraversable (Join (Flip p)) where
  sA' (W (F x)) =
    (m reFlip . sA' . W) x

  tr' func (W (F x)) =
    (m reFlip . tr' func . W) x

instance
  ( Traversable t1
  , Traversable t2
  )
    => Traversable (Compose t1 t2) where
  sA (C xs) = m C (tr sA xs)

instance
  ( StrongTraversable t1
  , StrongTraversable t2
  )
    => StrongTraversable (Compose t1 t2) where
  sA' (C xs) = m C (tr' sA' xs)
