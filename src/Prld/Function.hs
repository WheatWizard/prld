module Prld.Function
  ( on
  , fo
  )
  where

import qualified Prelude
import Prelude
  (
  )

import Data.Function
  ( on
  )

import Prld.Flip
  ( f
  )

-- | The flip of 'on'.
-- Use as a shorthand for @'f' on@
fo :: (a -> b) -> (b -> b -> c) -> (a -> a -> c)
fo = f on
