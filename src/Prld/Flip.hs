{-# Language QuantifiedConstraints #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
module Prld.Flip
  ( Flippable
    ( f
    )
  , ff
  , fF
  , fFF
  )
  where

import qualified Prelude
import Prelude
  (
  )

import Prld.Functor
import Prld.Coerce

-- | For things that which can be "flipped".
--
-- This class is most useful for things that are function like.
-- However there are some other less useful instances included anyway.
--
-- Instances must satisfy the property:
--
-- prop> f . f = id
class Flippable p where
  -- | Generalization of 'Prelude.flip'
  f :: p a (p b c) -> p b (p a c)

instance Flippable (->) where
  f g x y = g y x 

instance Flippable (,) where
  f (x, (y, z)) = (y, (x, z))

instance Flippable (Flip (,)) where
  f (F (F (x, y), z)) = F (F (x, z), y)

instance
  ( Flippable p
  , forall a . Functor (p a)
  )
    => Flippable (Flip (Flip p)) where
  f = (lF . lF) (m (F . F) . f . m q)

-- | The flip of 'f'.
-- Use as a shorthand for @'f' 'f'@.
ff :: b -> (a -> b -> c) -> a -> c
ff = f f

fF ::
  ( Flippable (Flip p)
  , Functor (Flip p c)
  , Functor (Flip p b)
  )
    => p (p a b) c -> p (p a c) b
fF = lf (m q . f . m F)

-- | A slightly more general version of 'f' for doubly flipped functions.
--
-- 'f' requires that `p` be a 'Functor' over all types while @fFF@ requires only it be a 'Functor' over two specific types.
fFF ::
  ( Flippable p
  , Functor (p a) 
  , Functor (p b)
  )
    => Flip (Flip p) a (Flip (Flip p) b c) -> Flip (Flip p) b (Flip (Flip p) a c)
fFF = (lF . lF) (m (F . F) . f . m q)
