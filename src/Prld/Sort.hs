{-# Language FlexibleInstances #-}
module Prld.Sort
  ( cp
  , op
  , Sortable
    ( sr
    , so
    , sb
    )
  , Ordering (..)
  )
  where

import qualified Prelude
import Prelude
  ( Ordering (..)
  , compare
  , Ord
  )

import qualified Data.List as List
import qualified Data.List.NonEmpty as NonEmpty

import Prld.Function
import Prld.Functor
import Prld.Containers

cp :: Ord a => a -> a -> Ordering
cp = compare

cP :: Ord b => (a -> b) -> a -> a -> Ordering
cP f = cp `on` f

op :: Ordering -> Ordering
op GT = LT
op EQ = EQ
op LT = GT

class Container f => Sortable f where
  -- | Sorts a container
  sr :: Ord a => f a -> f a
  sr = sb cp
  -- | Sort a container by comparing the results of a key function applied to each element.
  so :: Ord b => (a -> b) -> f a -> f a
  so f = sb (cP f)
  -- | Sort a container with a particular comparison.
  sb :: (a -> a -> Ordering) -> f a -> f a

instance Sortable [] where
  sr = List.sort
  so = List.sortOn
  sb = List.sortBy

-- | Converts a function on 'Prld.NonEmpty' to one on 'NonEmpty.NonEmpty'.
--
-- Not exported since we don't export 'NonEmpty.NonEmpty'.
liftNonEmpty ::
  (NonEmpty.NonEmpty a -> NonEmpty.NonEmpty a) -> (NonEmpty [] a -> NonEmpty [] a)
liftNonEmpty f (a :| b) =
  let
    (a' NonEmpty.:| b') = f (a NonEmpty.:| b)
  in
    a' :| b'

instance Sortable (NonEmpty []) where
  sr = liftNonEmpty NonEmpty.sort
  so f = liftNonEmpty (NonEmpty.sortWith f)
  sb f = liftNonEmpty (NonEmpty.sortBy f)

instance Sortable Vacuous where
  sr _ = V
  so _ _ = V
  sb _ _ = V

instance Sortable Identity where
  sr x = x
  so _ x = x
  sb _ x = x

instance
  ( Sortable f
  , Sortable g
  )
    => Sortable (Sum f g)
  where
    sr (Il x) = Il (sr x)
    sr (Ir x) = Ir (sr x)
    so f (Il x) = Il (so f x)
    so f (Ir x) = Ir (so f x)
    sb f (Il x) = Il (sb f x)
    sb f (Ir x) = Ir (sb f x)

instance
  ( Sortable f
  , Sortable g
  )
    => Sortable (Product f g)
  where
    sr (Pr x y) = Pr (sr x) (sr y)
    so f (Pr x y) = Pr (so f x) (so f y)
    sb f (Pr x y) = Pr (sb f x) (sb f y)

instance
  ( Sortable (f a)
  , Sortable (g a)
  )
    => Sortable (Bisum f g a)
  where
    sr (IL x) = IL (sr x)
    sr (IR x) = IR (sr x)
    so f (IL x) = IL (so f x)
    so f (IR x) = IR (so f x)
    sb f (IL x) = IL (sb f x)
    sb f (IR x) = IR (sb f x)

instance
  ( Sortable (f a)
  , Sortable (g a)
  )
    => Sortable (Biproduct f g a)
  where
    sr (PR x y) = PR (sr x) (sr y)
    so f (PR x y) = PR (so f x) (so f y)
    sb f (PR x y) = PR (sb f x) (sb f y)
