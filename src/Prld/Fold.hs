{-# Language BangPatterns #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language QuantifiedConstraints #-}
{-# Language UndecidableInstances #-}
{-# Language ScopedTypeVariables #-}
module Prld.Fold
  ( Foldable
    ( fR
    , fL
    , f'
    , r1
    )
  , l1
  , cn
  , c'
  , l
  , sL
  , sR
  )
  where

import qualified Prelude
import Prelude
  ( Num
    ( (+)
    )
  , Bool
    ( True
    , False
    )
  )
import Data.List as List

import Prld.Containers
import Prld.Functor
import Prld.Flip
import Prld.Monad

class Container t => Foldable t where
  {-# Minimal fR, fL, f' #-}
  fR :: (a -> b -> b) -> b -> t a -> b
  fL :: (b -> a -> b) -> b -> t a -> b
  f' :: (b -> a -> b) -> b -> t a -> b
  r1 :: (a -> a -> a) -> (NonEmpty t) a -> a
  r1 f (x :| xs) = fR go id xs x
    where
      id x = x
      go x r prev = f prev (r x)

instance Foldable [] where
  fR = Prelude.foldr

  fL = Prelude.foldl

  f' = List.foldl'

  r1 g = go
   where
     go (x1 :| []) = x1
     go (x1 :| (x2 : xs)) = g x1 (go (x2 :| xs))

instance
  ( Foldable f
  )
    => Foldable (NonEmpty f) where
  fR g x1 (x2 :| xs) = g x2 (fR g x1 xs)

  fL g x1 (x2 :| xs) = fL g (g x1 x2) xs 

  f' g !x1 (x2 :| xs) = fL g (g x1 x2) xs

  r1 g (x :| xs) = g x (r1 g xs)

instance Foldable Vacuous where
  fR g x V = x

  fL g x V = x

  f' g !x V = x

  r1 g (x :| V) = x

instance Foldable Identity where
  fR g x1 (I x2) = g x2 x1

  fL g x1 (I x2) = g x1 x2

  f' g !x1 (I x2) = g x1 x2

  r1 g (x1 :| (I x2)) = g x1 x2

instance Foldable IList where
  fR g _ = go
    where
      go (x2 :~ xs) = g x2 (go xs)

  fL g = go
    where
      go x1 (x2 :~ xs) = go (g x1 x2) xs

  f' g = go
    where
      go !x1 (x2 :~ xs) = go (g x1 x2) xs

  r1 g = go
    where
      go (x1 :| ~(x2 :~ xs)) = g x1 (go (x2 :| xs))

instance Foldable ((,) a) where
  fR g x1 (_, x2) = g x2 x1

  fL g x1 (_, x2) = g x1 x2

  f' g !x1 (_, x2) = g x1 x2

  r1 g (x1 :| (_, x2)) = g x1 x2

instance Foldable (Flip (,) a) where
  fR g x1 (F (x2, _)) = g x2 x1

  fL g x1 (F (x2, _)) = g x1 x2

  f' g !x1 (F (x2, _)) = g x1 x2

  r1 g (x1 :| (F (x2, _))) = g x1 x2

instance
  ( forall a . Foldable (p a)
  , forall b . Foldable (Flip p b)
  )
    => Foldable (Join p) where
  fR g x (W xs) = fR g (fR g x xs) (F xs)

  fL g x (W xs) = fL g (fL g x (F xs)) xs

  f' g !x (W xs) = f' g (f' g x (F xs)) xs

  r1 g (x :| W xs) = r1 g ((r1 g (x :| xs)) :| (F xs))

instance Foldable Maybe where
  fR g x N = x
  fR g x1 (J x2) = g x2 x1

  fL g x N = x
  fL g x1 (J x2) = g x1 x2

  f' g !x N = x
  f' g !x1 (J x2) = g x1 x2

  r1 g (x :| N) = x
  r1 g (x1 :| (J x2)) = g x1 x2

instance Foldable (Either a) where
  fR g x (L y) = x
  fR g x1 (R x2) = g x2 x1

  fL g x (L y) = x
  fL g x1 (R x2) = g x1 x2

  f' g !x (L y) = x
  f' g !x1 (R x2) = g x1 x2

  r1 g (x :| (L y)) = x
  r1 g (x1 :| (R x2)) = g x1 x2

instance Foldable (Flip Either a) where
  fR g x (F (R y)) = x
  fR g x1 (F (L x2)) = g x2 x1

  fL g x (F (R y)) = x
  fL g x1 (F (L x2)) = g x1 x2

  f' g !x (F (R y)) = x
  f' g !x1 (F (L x2)) = g x1 x2

  r1 g (x :| (F (R y))) = x
  r1 g (x1 :| (F (L x2))) = g x1 x2

instance
  ( Foldable (p a)
  )
    => Foldable (Flip (Flip p) a) where
  fR g x (F (F xs)) = fR g x xs

  fL g x (F (F xs)) = fL g x xs

  f' g !x (F (F xs)) = f' g x xs

  r1 g (x :| (F (F xs))) = r1 g (x :| xs)

instance
  ( Foldable f
  , Foldable g
  )
    => Foldable (Compose f g) where
  fR g x (C xs) = fR (f (fR g)) x xs

  fL g x (C xs) = fL (fL g) x xs

  f' g !x (C xs) = f' (f' g) x xs

instance
  ( Foldable f
  , Foldable (p a)
  )
    => Foldable (Bicompose f p a) where
  fR g x (Bc xs) = fR (f (fR g)) x xs

  fL g x (Bc xs) = fL (fL g) x xs

  f' g !x (Bc xs) = f' (f' g) x xs

instance
  ( Foldable f
  , Foldable (Flip p a)
  )
    => Foldable (Flip (Bicompose f p) a) where
  fR g x (F (Bc xs)) = fR (f (fR g)) x (m F xs)

  fL g x (F (Bc xs)) = fL (fL g) x (m F xs)

  f' g !x (F (Bc xs)) = f' (f' g) x (m F xs)

instance
  ( Foldable f
  , Foldable (p (f a))
  )
    => Foldable (On p f a) where
  fR g x (On xs) = fR (f (fR g)) x xs

  fL g x (On xs) = fL (fL g) x xs

  f' g !x (On xs) = f' (f' g) x xs

instance
  ( Foldable f
  , Foldable (Flip p (f a))
  )
    => Foldable (Flip (On p f) a) where
  fR g x (F (On xs)) = fR (f (fR g)) x (F xs)

  fL g x (F (On xs)) = fL (fL g) x (F xs)

  f' g !x (F (On xs)) = f' (f' g) x (F xs)

instance
  ( Foldable f
  , Foldable g
  )
    => Foldable (Sum f g) where
  fR g x (Ir xs) = fR g x xs
  fR g x (Il xs) = fR g x xs

  fL g x (Ir xs) = fL g x xs
  fL g x (Il xs) = fL g x xs

  f' g !x (Ir xs) = f' g x xs
  f' g !x (Il xs) = f' g x xs

  r1 g (x :| Ir xs) = r1 g (x :| xs)
  r1 g (x :| Il xs) = r1 g (x :| xs)

instance
  ( Foldable (p a)
  , Foldable (q a)
  )
    => Foldable (Bisum p q a) where
  fR g x (IR xs) = fR g x xs
  fR g x (IL xs) = fR g x xs

  fL g x (IR xs) = fL g x xs
  fL g x (IL xs) = fL g x xs

  f' g !x (IR xs) = f' g x xs
  f' g !x (IL xs) = f' g x xs

  r1 g (x :| IR xs) = r1 g (x :| xs)
  r1 g (x :| IL xs) = r1 g (x :| xs)

instance
  ( Foldable (Flip p a)
  , Foldable (Flip q a)
  )
    => Foldable (Flip (Bisum p q) a) where
  fR g x (F (IR xs)) = fR g x (F xs)
  fR g x (F (IL xs)) = fR g x (F xs)

  fL g x (F (IR xs)) = fL g x (F xs)
  fL g x (F (IL xs)) = fL g x (F xs)

  f' g !x (F (IR xs)) = f' g x (F xs)
  f' g !x (F (IL xs)) = f' g x (F xs)

  r1 g (x :| F (IR xs)) = r1 g (x :| F xs)
  r1 g (x :| F (IL xs)) = r1 g (x :| F xs)

l1 ::
  ( Foldable t
  )
    => (a -> a -> a) -> (NonEmpty t) a -> a
l1 g (x :| xs) = fL g x xs

-- | Counts the number of elements satisfying a predicate.
cn ::
  ( Foldable f
  , Num b
  )
    => (a -> Bool) -> f a -> b
cn = c' 0

-- | Counts the number of elements starting from a value.
-- It is eager on this start value.
c' ::
  ( Foldable f
  , Num b
  )
    => b -> (a -> Bool) -> f a -> b
c' !n f = f' test n
  where
    test x y
      | f y = x + 1
    test x y = x

-- | Gives the size of a container.
-- Replaces `Prelude.length`
l ::
  ( Foldable f
 , Num b
  )
    => f a -> b
l = cn (\ x -> True)

sL :: forall a b f .
  ( Foldable f
  )
    => (b -> a -> b) -> b -> f a -> NonEmpty [] b
sL g b as = fL addOn (b :| []) as
  where
    addOn :: NonEmpty [] b -> a -> NonEmpty [] b
    addOn (q :| qs) u = g q u :| (q : qs)

sR :: forall a b f .
  ( Foldable f
  )
    => (a -> b -> b) -> b -> f a -> NonEmpty [] b
sR g b as = b :| r (fR addOn [] as)
  where
    addOn :: a -> [b] -> [b]
    addOn u (c : cs) = g u c : c : cs
    addOn u [] = [g u b]
        

