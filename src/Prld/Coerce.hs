module Prld.Coerce
  ( q
  , lq
  , Coercible
  )
  where

import Prelude as Prelude
import Prelude
  ( )

import Data.Coerce
import GHC.Exts
  ( RuntimeRep
  , TYPE
  )

-- | Short alias for 'Data.Coerce.coerce' without levity polymorphism. Can only be used with kind @*@.
q :: Coercible a b => a -> b 
q = coerce

-- | A lifts a function onto a coercible type.
--
-- Coerces, applies a function, then coerces back.
lq :: Coercible a b => (a -> a) -> (b -> b)
lq func = q . func . q
