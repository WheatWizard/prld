{-# Language MultiParamTypeClasses #-}
module Prld.Set
  ( Set
  , sm
  , ef
  )
  where

import qualified Prelude
import Prelude
  ( Ord
  )

import Data.Set

import Prld.Functor
import Prld.Containers
import Prld.Bool


-- | Has the right type but behaves strangely with sets use 'sm' instead most the time. 
instance Functor Set where
  m = mapMonotonic

-- | A proper map for 'Set'.
--
-- The builtin map is only safe for certain purposes since it will cause the set to be out of order if the given function is not monotonic.
sm :: Ord b => (a -> b) -> Set a -> Set b
sm = map

-- | Slow, use 'ef' for better perfomance.
instance Container Set where
  e x = e x . toList
  ay x = ay x . toList

instance Filtrable Set Set where
  fl = filter

{-
instance Sized Set where
  cn s =
    case
      splitRoot s
    of
      [] ->
        0
      [left, root, right] ->
        1 + cn left + cn right
-}

-- | A fast version of 'e' for when the set contains an orderable type.
-- 
-- Does not fulfill the container property
--
-- prop> ef thing set = ef (func thing) (m func set)
--
-- Unless @func@ is monotonic.
--
-- But does fulfill the analygous property
--
-- prop> e thing set = e (func thing) (sm func set)
ef :: Ord a => a -> Set a -> Bool
ef = member
