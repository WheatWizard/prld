{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language UndecidableInstances #-}
{-# Language QuantifiedConstraints #-}
{-# Language InstanceSigs #-}
module Prld.Distributive
  ( Distributive
    ( di
    , cl
    )
  , Bidistributive
    ( uz
    , bC
    )
  )
  where

import Prld.Functor
import Prld.Containers
import Prld.Coerce

import qualified Prelude
import Prelude
  ( )

class Functor g => Distributive g where
  {-# Minimal di | cl #-}
  di ::
    ( Functor f
    )
      => f (g a) -> g (f a)
  di = cl (\ x -> x)
  cl ::
    ( Functor f
    )
      => (a -> g b) -> f a -> g (f b)
  cl f x = di (m f x)

class
  ( forall a. Functor (p a)
  , forall a. Functor (Flip p a)
  )
    => Bidistributive p where
  {-# Minimal uz | bC #-}
  -- | Bidistribute or a generalized 'Prelude.unzip'.
  uz ::
    ( Functor f
    )
      => f (p a b) -> p (f a) (f b)
  uz = bC (\ x -> x)
  -- | Bicollect or a generalized @unzipWith@.
  bC ::
    ( Functor f
    )
      => (a -> p b c) -> f a -> p (f b) (f c)
  bC f x = uz (m f x)

-- Distributive Instances --

instance Distributive Vacuous where
  di xs = V
  cl f xs = V

instance Distributive ((->) a) where
  di fs e = m (\ f -> f e) fs

instance Distributive Identity where
  di x = I (m (\ (I k) -> k) x)

instance Bidistributive p => Distributive (p ()) where
  di x = m2 (\ x -> ()) (uz x)

instance Distributive IList where
  di x = m head x :~ di (m tail x)
    where
      tail (x :~ xs) = xs
      head (x :~ xs) = x

instance Distributive RIList where
  di xs = m last xs :* di (m init xs)
    where
      init (x :* xs) = xs
      last (x :* xs) = x

instance Distributive g => Distributive (NonEmpty g) where
  di xs = m head xs :| di (m tail xs)
    where
      head (x :| xs) = x
      tail (x :| xs) = xs

instance
  ( Distributive f
  , Distributive g
  )
    => Distributive (Compose f g) where
  di = C . m di . cl q

instance
  ( Distributive f
  , Distributive (p a)
  )
    => Distributive (Bicompose f p a) where
  di = Bc . m di . cl q

instance
  ( Distributive f
  , Distributive (Flip p a)
  )
    => Distributive (Flip (Bicompose f p) a) where
  di :: Functor g => g (Flip (Bicompose f p) a b) -> Flip (Bicompose f p) a (g b)
  di = F . Bc . m q . m di . di . mm F . m (\ (F (Bc x)) -> x)

instance
  ( Distributive f
  , Distributive (p (f a))
  )
    => Distributive (On p f a) where
  di = On . m di . cl q

instance
  ( Distributive f
  , Distributive (Flip p (f a))
  )
    => Distributive (Flip (On p f) a) where
  di :: Functor g => g (Flip (On p f) a b) -> Flip (On p f) a (g b)
  di = F . On . q . m di . di . m F . m (\ (F (On x)) -> x)

instance
  ( Distributive f
  , Distributive g
  )
    => Distributive (Product f g) where
  di xs = Pr (di (m getLeft xs)) (di (m getRight xs))
    where
      getLeft (Pr x y) = x
      getRight (Pr x y) = y

instance Bidistributive p => Distributive (Join p) where
  di = W . uz . m q
  cl f = W . bC (q . f)

instance Distributive (p a) => Distributive (Flip (Flip p) a) where
  di = F . F . di . m q

-- Bidistributive Instances --

instance Bidistributive (,) where
  uz xs = (m fst xs, m snd xs)
    where
      fst (a, b) = a
      snd (a, b) = b

instance
  ( Bidistributive p
  , Distributive f
  )
    => Bidistributive (Bicompose f p) where
  uz = Bc . m uz . di . m q

instance
  ( Bidistributive p
  , Distributive f
  )
    => Bidistributive (On p f) where
  uz = On . bm di di . uz . m q

instance
  ( Bidistributive p
  , Bidistributive q
  )
    => Bidistributive (Biproduct p q) where
  uz xs =  PR (uz (m getLeft xs)) (uz (m getRight xs))
    where
      getLeft (PR l r) = l
      getRight (PR l r) = r

instance
  ( Bidistributive p
  )
    => Bidistributive (Flip p) where
  uz = F . uz . m q
