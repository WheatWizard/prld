{-# Language KindSignatures #-}
{-# Language ConstraintKinds #-}
{-# Language FlexibleContexts #-}
{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
{-# Language QuantifiedConstraints #-}
{-# Language ImpredicativeTypes #-}
{-# Language PolyKinds #-}
module Prld.Functor.Combinators
  (
  -- * Functor combinators
    Flip
    ( F
    )
  , lF, lf, uF
  , Compose
    ( C
    )
  , Sum
    ( Il
    , Ir
    )
  , Product
    ( Pr
    )
  , Bicompose
    ( Bc
    )
  , On
    ( On
    )
  , Bisum
    ( IL
    , IR
    )
  , Biproduct
    ( PR
    )
  , Join
    ( W
    )
  , lW, uW
  )
  where

import qualified Prelude 
import Prelude
  ( Show
  -- (.) is compose not fmap since functors have not been defined yet in this module
  , (.)
  )

import Prld.Coerce

import Data.Kind

-- | Flips a bifunctor.
--
-- This is the C combinator on types.
newtype
  Flip
    (f :: k -> j -> Type)
    (a :: j)
    (b :: k)
  = F (f b a)

uF :: Flip f a b -> f b a
uF (F x) = x

lF :: (f b a -> g d c) -> Flip f a b -> Flip g c d
lF func = F . func . q

lf :: (Flip f a b -> Flip g c d) -> f b a -> g d c
lf func = q . func . F

-- | The right-to-left composition of functors.
--
-- This is the B combinator on types.
newtype
  Compose
    (f :: k -> Type)
    (g :: j -> k)
    (a :: j)
  = C (f (g a))

-- | Lifted sum of functors.
data
  Sum
    (f :: k -> Type)
    (g :: k -> Type)
    (a :: k)
  = Il (f a)
  | Ir (g a)

-- | Lifted product of functors.
data
  Product
    (f :: k -> Type)
    (g :: k -> Type)
    (a :: k)
  = Pr (f a) (g a)

-- | The right-to-left composition of a functor with a bifunctor.
--
-- This is the blackbird combinator on types.
newtype
  Bicompose
    (f :: k -> Type)
    (p :: j -> i -> k)
    (a :: j)
    (b :: i)
  = Bc (f (p a b))

-- | The composition of a functor across both holes of a bifunctor
-- 
-- This is the Haskell function 'on' on types.
newtype
  On
    (p :: k -> k -> Type)
    (f :: j -> k)
    (a :: j)
    (b :: j)
  = On (p (f a) (f b))

-- | Lifted sum of bifunctors.
data
  Bisum
    (p :: k -> j -> Type)
    (q :: k -> j -> Type)
    (a :: k)
    (b :: j)
  = IL (p a b)
  | IR (q a b)

-- | Lifted product of bifunctors.
data
  Biproduct
    (p :: k -> j -> Type)
    (q :: k -> j -> Type)
    (a :: k)
    (b :: j)
  = PR (p a b) (q a b)

-- | Make a 'Functor' over both arguments of a 'Bifunctor'.
-- This is the W combinator on types.
newtype
  Join
    (f :: k -> k -> Type)
    (a :: k)
  = W (f a a)

-- | Inverse of the 'W' constructor.
uW :: Join p a -> p a a
uW (W x) = x

-- | Lifts an operation onto joins.
lW :: (p a a -> q b b) -> Join p a -> Join q b
lW func (W x) = W (func x)

instance Show (f a a) => Show (Join f a) where
  show (W x) = Prelude.show x

