module Prld.Bool
  ( Boolish
    ( bt
    , tp
    , n
    , (&&)
    , (||)
    , (^^)
    , (~&)
    , (~|)
    , (~^)
    , tB
    )
  , dB
  , Any
    ( A
    )
  , XAny
    ( Xa
    )
  , Bool
    ( True
    , False
    )
  )
    where

import qualified Prelude
import Prelude
  ( Bool
    ( True
    , False
    )
  , Eq
  )

-- | For types that are isomorphic to Booleans
-- 
-- Although there are several minimal complete definitions,
-- for performance reasons one should seek to implement all member functions
class Eq a => Boolish a where
  {-# Minimal
    (~&), tp, tB
    | (~&), bt, tB
    | (~|), tp, tB
    | (~|), bt, tB
    | n, (&&), tp, tB
    | n, (&&), bt, tB
    | n, (||), tp, tB
    | n, (||), bt, tB
    #-}
  -- | The false symbol
  bt :: a 
  bt = n tp
  -- | The true symbol
  tp :: a
  tp = n bt
  -- | Logical negation
  n :: a -> a
  n x = x ~& x
  -- | Logical and
  (&&) :: a -> a -> a
  x && y = n (n x || n y)
  -- | Logical or
  (||) :: a -> a -> a
  x || y = (n x ~& n y)
  -- | Logical xor
  (^^) :: a -> a -> a
  x ^^ y = (x ~& xNANDy ) ~& (y ~& xNANDy)
    where
      xNANDy = x ~& y
  -- | Logical nand
  (~&) :: a -> a -> a
  x ~& y = not (not x ~| not y)
   where
     not z = z ~| z 
  -- | Logical nor
  (~|) :: a -> a -> a 
  x ~| y = n x && n y
  -- | Logical xnor
  (~^) :: a -> a -> a
  x ~^ y = (x ~| xNORy) ~| (x ~| xNORy)
    where
      xNORy = x ~| y
  -- | Converts to a Boolean
  tB :: a -> Bool

dB :: Boolish a => Bool -> a
dB x
  | x   = tp
  | n x = bt

instance Boolish Bool where
  n = Prelude.not

  (&&) = (Prelude.&&)

  (||) = (Prelude.||)

  False ~& _     = True
  _     ~& False = True
  True  ~& True  = False

  True  ~| _     = False
  _     ~| True  = False
  False ~| False = True

  True  ^^ x = n x
  False ^^ x = x

  False ~^ x = n x
  True  ~^ x = x

  tp = True
  
  bt = False

  tB x = x

-- | Isomorphic to 'Bool' but with a different instance of Semigroup
-- For a regular Bool
--
-- prop> (<>) = (&&)
--
-- but here
--
-- prop> (<>) = (||)
newtype Any = A Bool
  deriving
    ( Eq
    )

instance Boolish Any where
  tp = A True
  bt = A False
  n ~(A x) = A (n x)
  (&&) ~(A x) ~(A y) = A (x && y)
  (||) ~(A x) ~(A y) = A (x || y)
  (^^) ~(A x) ~(A y) = A (x ^^ y)
  (~&) ~(A x) ~(A y) = A (x ~& y)
  (~|) ~(A x) ~(A y) = A (x ~| y)
  (~^) ~(A x) ~(A y) = A (x ~^ y)
  tB (A x) = x
  

-- | Isomorphic to 'Bool' but with a different instance of Semigroup
-- For a regular Bool
--
-- prop> (<>) = (&&)
--
-- but here
--
-- prop> (<>) = (^^)
newtype XAny = Xa Bool
  deriving
    ( Eq
    )

instance Boolish XAny where
  tp = Xa True
  bt = Xa False
  n ~(Xa x) = Xa (n x)
  (&&) ~(Xa x) ~(Xa y) = Xa (x && y)
  (||) ~(Xa x) ~(Xa y) = Xa (x || y)
  (^^) ~(Xa x) ~(Xa y) = Xa (x ^^ y)
  (~&) ~(Xa x) ~(Xa y) = Xa (x ~& y)
  (~|) ~(Xa x) ~(Xa y) = Xa (x ~| y)
  (~^) ~(Xa x) ~(Xa y) = Xa (x ~^ y)
  tB (Xa x) = x
