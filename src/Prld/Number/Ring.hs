{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
module Prld.Number.Ring
  ( Rng
  , Ring
  , Rng1
  )
  where

import Prld.Number.Semigroup
import Prld.Number.Monoid
import Prld.Number.Group

import Prld.Bool

-- | A non-unital ring or "rng"
class
  ( Group a
  , Semigroup' a
  )
    => Rng a

class
  ( Rng a
  , Monoid' a
  )
    => Ring a

instance
  ( Rng a
  , Monoid' a
  )
    => Ring a

class Group1 f => Rng1 f

-- Rng instances --

instance Rng XAny

instance
  ( Rng1 f
  , Rng a
  )
    => Rng (f a)

-- Rng1 instances

instance Rng1 ((->) a)
