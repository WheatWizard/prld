{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
module Prld.Number.Monoid
  ( Monoid
    ( me
    )
  , Monoid'
    ( mE
    )
  )
    where

import qualified Prelude
import Prelude
  (
  )

import Prld.Number.Semigroup
import Prld.Bool
import Prld.Monad

-- | Our version of 'Prelude.Monoid'
class Semigroup a => Monoid a where
  me :: a

class Semigroup' a => Monoid' a where
  mE :: a

{-
-- | Lifted class of 'Monoid'
class Semigroup1 f => Monoid1 f where
  li :: a -> f a
-}

-- Monoid instances --

instance Monoid () where
  me = ()

instance Monoid Bool where
  me = True

instance Monoid Any where
  me = A False

instance Monoid XAny where
  me = Xa False

instance
  ( Applicative f
  , Monoid a
  )
    => Monoid (f a) where
  me = p me

-- Monoid' instances --

instance Monoid' () where
  mE = ()

instance Monoid' Bool where
  mE = False

instance Monoid' XAny where
  mE = Xa True

instance
  ( Applicative f
  , Monoid' a
  )
    => Monoid' (f a) where
  mE = p mE

