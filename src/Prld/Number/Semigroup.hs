{-# Language UndecidableInstances #-}
{-# Language FlexibleInstances #-}
module Prld.Number.Semigroup
  ( Semigroup
    ( (<>)
    )
  , Semigroup'
    ( (><)
    )
  , sg
  )
    where

import qualified Prelude
import Prelude
  ( Num
    ( (+)
    , (*)
    )
  )

import Prld.Bool
import Prld.Monad

-- | Our version of 'Prelude.Semigroup'.
class Semigroup a where
  (<>) :: a -> a -> a

-- | A second version of 'Prelude.Semigroup'.
-- Used for building Rings.
class Semigroup' a where
  (><) :: a -> a -> a

-- | A prefix version of '(<>)'.
sg ::
  ( Semigroup a
  )
    => a -> a -> a
sg = (<>)

{-
-- Not needed since it is basically equivalent to applicative
-- | An applicative such that if
-- an arbitrary operation @binop@ is associative, then @liftA2 binop@ is also associative.
class Apply f => Semigroup1 f
-}

-- Semigroup instances --

instance Semigroup () where
  _ <> _ = ()

instance Semigroup Bool where
  (<>) = (&&)

instance Semigroup Any where
  (<>) = (||)

instance Semigroup XAny where
  (<>) = (^^)

instance
  ( Applicative f -- Need to check if this can be weakened to `Apply f f f` once I give `Apply` laws.
  , Semigroup a
  )
    => Semigroup (f a) where
  (<>) = l2 (<>)

-- Semigroup' instances --

instance Semigroup' () where
  _ >< _ = ()

instance Semigroup' Bool where
  (><) = (||)

instance Semigroup' XAny where
  (><) = (&&)

instance
  ( Applicative f
  , Semigroup' a
  )
    => Semigroup' (f a) where
  (><) = l2 (><)
