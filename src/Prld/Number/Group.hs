{-# Language FlexibleInstances #-}
module Prld.Number.Group
  ( Group
    ( ng
    )
  , Group1
    ( ln
    )
  )
    where

import qualified Prelude
import Prelude
  (
  )

import Prld.Number.Monoid
import Prld.Bool
import Prld.Monad

class Monoid g => Group g where
  ng :: g -> g

class Applicative f => Group1 f where
  ln :: (a -> a) -> f a -> f a

-- Group instances --

instance Group () where
  ng _ = ()

instance Group XAny where
  ng = n

instance
  ( Group a
  , Group1 f
  )
    => Group (f a) where
  ng = ln ng 

-- Group1 instances --

instance Group1 ((->) a) where
  ln f g x = f (g x)
