{-# Language KindSignatures #-}
{-# Language ConstraintKinds #-}
{-# Language FlexibleContexts #-}
{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
{-# Language QuantifiedConstraints #-}
{-# Language ImpredicativeTypes #-}
{-# Language PolyKinds #-}
module Prld.Functor
  ( module Prld.Functor.Combinators
  -- * Classes
  , Functor
    ( m
    )
  , (.), fm, (...), mm
  , Contravariant
    ( (>$)
    , contramap
    )
  , cm, mcm
  , Bifunctor
    ( bm'
    )
  , m2
  , bm
  , Profunctor
    ( dM
    )
  , lm
  , dm
  -- * Lifted Classes
  , Eq1
    ( lE
    )
  , (=^)
  , (/=^)
  , Ord1
    ( lC
    )
  , (<^)
  , (>^)
  , (<=^)
  , (>=^)
  )
  where

import Data.Kind
import Data.Functor.Contravariant
  ( Contravariant
    ( contramap
    , (>$)
    )
  )
import Prld.Coerce
  ( q
  )
import Prld.Functor.Combinators

import qualified Prelude
import Prelude
  ( Bool
    ( True
    , False
    )
  , Ordering
    ( GT
    , LT
    , EQ
    )
  , IO
  , (==)
  , (/=)
  , not
  , (&&)
  )

-- | A type @f@ is a 'Functor' if it provides a function map ('m') which,
-- makes any function @(a -> b)@ into a function @f a -> f b@, which preserves the structure of f.
--
-- 'm' must to adhere to the following:
--
-- [/identity/]
--
-- prop> m id = id
--
-- [/composition/]
--
-- prop> m (f . g) = m f . m g
--
-- Note that if the first law is held the second law is implied, because of a free theorem.
class Functor f where
  -- | A map.
  -- Replaces 'Prelude.fmap'.
  m :: (a -> b) -> f a -> f b

instance
  ( Functor (p a)
  )
    => Functor (Flip (Flip p) a) where
  m f = F . F . m f . q 

instance
  ( Functor f
  , Functor g
  )
    => Functor (Compose f g) where
  m f (C x) = C (mm f x)

instance
  ( Functor f
  , Functor g
  )
    => Functor (Sum f g) where
  m f (Il x) = Il (m f x)
  m f (Ir x) = Ir (m f x)

instance
  ( Functor f
  , Functor g
  )
    => Functor (Product f g) where
  m f (Pr x y) = Pr (m f x) (m f y)

instance
  ( Functor f
  , Functor (p a)
  )
    => Functor (Bicompose f p a) where
  m f (Bc x) = Bc (mm f x)

instance
  ( Functor f
  , Functor (p (f a))
  )
    => Functor (On p f a) where
  m f (On x) = On (mm f x)

instance
  ( Functor (p a)
  , Functor (q a)
  )
    => Functor (Bisum p q a) where
  m f (IL x) = IL (m f x)
  m f (IR x) = IR (m f x)

instance
  ( Functor (p a)
  , Functor (q a)
  )
    => Functor (Biproduct p q a) where
  m f (PR x y) = PR (m f x) (m f y)

instance
  ( forall a . Functor (p a)
  , forall a . Functor (Flip p a)
  )
    => Functor (Join p) where
  m f (W x) = W (bm f f x)

instance Functor ((->) r) where
  m f g x = f (g x)
  {-# Inline m #-}

instance Functor IO where
  m = Prelude.fmap

instance Functor [] where
  m = Prelude.fmap

instance Functor ((,) a) where
  m f ~(a, b) = (a, f b)

instance Functor (Flip (,) b) where
  m f ~(F (a, b)) = F (f a, b)

instance Functor ((,,) a b) where
  m f ~(a, b, c) = (a, b, f c)

instance Functor (Flip ((,,) a) c) where
  m f ~(F (a, b, c)) = F (a, f b, c)

instance Functor ((,,,) a b c) where
  m f ~(a, b, c, d) = (a, b, c, f d)

instance Functor (Flip ((,,,) a b) d) where
  m f ~(F (a, b, c, d)) = F (a, b, f c, d)

instance Functor ((,,,,) a b c d) where
  m f ~(a, b, c, d, e) = (a, b, c, d, f e)

instance Functor (Flip ((,,,,) a b c) e) where
  m f ~(F (a, b, c, d, e)) = F (a, b, c, f d, e)

instance
  ( Functor f
  , Functor (Flip p a)
  )
    => Functor (Flip (Bicompose f p) a) where
  m f (F (Bc x)) = F (Bc (m2 f . x)) 

instance
  ( Functor f
  , Functor (Flip p (f a))
  )
    => Functor (Flip (On p f) a) where
  m f (F (On x)) = F (On (q (mm f (F x))))

instance
  ( Functor (Flip p b)
  , Functor (Flip q b)
  )
    => Functor (Flip (Bisum p q) b) where
  m f (F (IL x)) = F (IL (m2 f x))
  m f (F (IR x)) = F (IR (m2 f x))

instance
  ( Functor (Flip p b)
  , Functor (Flip q b)
  )
    => Functor (Flip (Biproduct p q) b) where
  m f (F (PR x y)) = F (PR (m2 f x) (m2 f y))

instance
  ( Functor f
  , Contravariant g
  )
    => Contravariant (Compose f g) where
  contramap f (C x) = C (m (cm f) x)
  b >$ (C x)        = C (m (b >$) x)


instance
  ( Functor f
  , Contravariant (p a)
  )
    => Contravariant (Bicompose f p a) where
  contramap f (Bc x) = Bc (m (cm f) x)
  b >$ (Bc x)        = Bc (m (b >$) x)

instance
  ( Contravariant f
  , Functor (p (f a))
  ) 
    => Contravariant (On p f a) where
  contramap f (On x) = On (m (cm f) x)
  b >$ (On x) = On (m (b >$) x)

instance
  ( Contravariant f
  , Contravariant g
  )
    => Contravariant (Sum f g) where
  contramap f (Ir x) = Ir (cm f x)
  contramap f (Il x) = Il (cm f x)
  b >$ (Ir x) = Ir (b >$ x)
  b >$ (Il x) = Il (b >$ x)

instance
  ( Contravariant (p a)
  , Contravariant (q a)
  )
    => Contravariant (Bisum p q a) where
  contramap f (IR x) = IR (cm f x)
  contramap f (IL x) = IL (cm f x)
  b >$ (IR x) = IR (b >$ x)
  b >$ (IL x) = IL (b >$ x)

instance
  ( Contravariant f
  , Contravariant g
  )
    => Contravariant (Product f g) where
  contramap f (Pr x y) = Pr (cm f x) (cm f y)

instance
  ( Contravariant (p a)
  , Contravariant (q a)
  )
    => Contravariant (Biproduct p q a) where
  contramap f (PR x y) = PR (cm f x) (cm f y)

instance Contravariant (Flip (->) a) where
  contramap = lF . fm

instance
  ( Functor f
  , Contravariant (Flip p a)
  )
    => Contravariant (Flip (Bicompose f p) a) where
  contramap f (F (Bc x)) = F (Bc (m (lm f) x))

instance
  ( Contravariant f
  , Functor (Flip p (f a))
  )
    => Contravariant (Flip (On p f) a) where
  contramap f (F (On x)) = (F . On . m2 (cm f)) x

instance
  ( Contravariant (Flip p a)
  , Contravariant (Flip q a)
  )
    => Contravariant (Flip (Bisum p q) a) where
  contramap f (F (IL x)) = F (IL (lm f x))
  contramap f (F (IR x)) = F (IR (lm f x))

instance
  ( Contravariant (Flip p a)
  , Contravariant (Flip q a)
  )
    => Contravariant (Flip (Biproduct p q) a) where
  contramap f (F (PR x y)) = F (PR (lm f x) (lm f y))

-- | Generalization of '(Prelude..)', and nickname for '(Prelude.<$>)'.
-- It performs mapping as an infix.
-- For the prefix version see 'm'.
(.) :: Functor f => (a -> b) -> f a -> f b 
(.) = m
{-# Inline (.) #-}

infixr 9 .

-- | Generalization of the blackbird combinator.
-- Performs a map two levels deep.
--
-- Infix version of 'mm'.
--
-- Equivalent to @('.') . (.)@ or @'m' m m@.
--
-- Sometimes called @\<\<$\>\>@ or @\<$$\>@.
(...) ::
  ( Functor f
  , Functor g
  )
    => (a -> b) -> f (g a) -> f (g b)
(...) = (.) . (.)

infixr 9 ...

-- | Generalization of the blackbird combinator.
-- Performs a map two levels deep.
--
-- Prefix version of '(...)'.
--
-- Equivalent to @('.') . (.)@ or @'m' m m@.
mm ::
  ( Functor f
  , Functor g
  )
    => (a -> b) -> f (g a) -> f (g b)
mm = m m m

-- | Performs a contramap inside of a covariant functor.
--
-- Equivalent to @'m' '.' 'cm'@.
mcm ::
  ( Functor f
  , Contravariant g
  )
    => (b -> a) -> f (g a) -> f (g b)
mcm = m m cm


-- | 'Prelude.flip' of 'm'.
-- Use as a shorthand for 'Prld.f' 'm'
fm ::
  ( Functor f
  )
    => f a -> (a -> b) -> f b 
fm x f = m f x

-- | Nickname for 'contramap'
cm ::
  ( Contravariant f
  )
    => (a -> b) -> f b -> f a
cm = contramap

-- | A bifunctor.
-- You cannot make new instances for this class, instead make the required instances.
class
  ( (forall a . Functor (p a))      :: Constraint
  , (forall a . Functor (Flip p a)) :: Constraint
  ) => Bifunctor p where
  -- | A nickname for @bimap@.
  -- 'bm' constrainted strictly to 'Bifunctor'.
  bm' :: (a -> b) -> (c -> d) -> p a c -> p b d
  bm' = bm

instance
  ( (forall a . Functor (p a))      :: Constraint
  , (forall a . Functor (Flip p a)) :: Constraint
  ) => Bifunctor p

-- | A map performed on the first argument of a 'Bifunctor'.
m2 ::
  ( Functor (Flip p c)
  )
    => (a -> b) -> p a c -> p b c
m2 = lf . m

-- | A nickname for @bimap@.
-- Performs a maps on both the first and second arguments of a 'Bifunctor'.
--
-- Technically does not require 'p' to be a bifunctor.
-- For the more restrictive version see 'bm''
bm ::
  ( Functor (Flip p d)
  , Functor (p a)
  )
    => (a -> b) -> (c -> d) -> p a c -> p b d
bm f1 f2 = m2 f1 . m f2

-- | A profunctor.
-- You cannot make new instances for this class, instead make the required instances.
class
  ( (forall a . Functor       (p a))      :: Constraint
  , (forall a . Contravariant (Flip p a)) :: Constraint
  ) => Profunctor p where
  -- | Nickname for @dimap@.
  -- 'dm' constrainted strictly to 'Bifunctor'.
  dM :: (b -> a) -> (c -> d) -> p a c -> p b d
  dM = dm

instance
  ( (forall a . Functor       (p a))      :: Constraint
  , (forall a . Contravariant (Flip p a)) :: Constraint
  ) => Profunctor p

-- | A contravariant map performed on the first argument of a 'Profunctor'.
lm ::
  ( Contravariant (Flip p c)
  )
    => (b -> a) -> p a c -> p b c
lm = lf . cm

-- | A nickname for @dimap@.
-- Performs a maps on both the first and second arguments of a 'Profunctor'.
--
-- Technically does not require 'p' to be a profunctor.
-- For the more restrictive version see 'dM'
dm ::
  ( Contravariant (Flip p c)
  , Functor       (p b)
  )
    => (b -> a) -> (c -> d) -> p a c -> p b d
dm f1 f2 = m f2 . lm f1


{-
class (forall a . Bifunctor (p a)) => Trifunctor p where
  m3 :: (a -> b) -> p a c d -> p b c d

-- | A trimap
-- Performs maps on all of the arguments of a 'Trifunctor'.
tm ::
  ( Trifunctor p
  )
    => (a -> b) -> (c -> d) -> (e -> f) -> p a c e -> p b d f
tm f1 f2 f3 = m3 f1 . bm f2 f3
-}

-- | Mirror of 'Data.Functor.Classes.Eq1'.
class Eq1 f where
  -- | Nickname for 'Data.Functor.Classes.liftEq'.
  -- Lifts an equality test through the type constructor.
  --
  -- The function will usually be applied to an equality function, but the more general type ensures that the implementation uses it to compare elements of the first container with elements of the second.
  lE :: (a -> b -> Bool) -> f a -> f b -> Bool

instance
  ( Eq1 f
  , Eq1 g
  )
    => Eq1 (Compose f g) where
  lE eq (C x) (C y) = lE (lE eq) x y

instance
  ( Eq1 f
  , Eq1 (p a)
  )
    => Eq1 (Bicompose f p a) where
  lE eq (Bc x) (Bc y) = lE (lE eq) x y

instance
  ( Eq1 f
  , Eq1 (p (f a))
  )
    => Eq1 (On p f a) where
  lE eq (On x) (On y) = lE (lE eq) x y

instance
  ( Eq1 f
  , Functor f
  , Eq1 (Flip p a)
  )
    => Eq1 (Flip (Bicompose f p) a) where
  lE cmp (F (Bc x)) (F (Bc y)) = lE (lE cmp) (m F x) (m F y)

instance
  ( Eq1 f
  , Eq1 (Flip p (f a))
  )
    => Eq1 (Flip (On p f) a) where
  lE cmp (F (On x)) (F (On y)) = lE (lE cmp) (F x) (F y)

instance
  ( Eq1 f
  , Eq1 g
  )
    => Eq1 (Sum f g) where
  lE eq (Ir x) (Ir y) = lE eq x y
  lE eq (Il x) (Il y) = lE eq x y
  lE _  _      _      = False

instance
  ( Eq1 (p a)
  , Eq1 (q a)
  )
    => Eq1 (Bisum p q a) where
  lE eq (IR x) (IR y) = lE eq x y
  lE eq (IL x) (IL y) = lE eq x y
  lE _  _      _      = False

instance
  ( Eq1 (Flip p a)
  , Eq1 (Flip q a)
  )
    => Eq1 (Flip (Bisum p q) a) where
  lE eq (F (IR x)) (F (IR y)) = lE eq (F x) (F y)
  lE eq (F (IL x)) (F (IL y)) = lE eq (F x) (F y)
  lE _  (F (IL x)) (F (IR y)) = False
  lE _  (F (IR x)) (F (IL y)) = False

instance Eq1 [] where
  lE eq []       []       = True
  lE eq (x : xs) (y : ys) = eq x y && lE eq xs ys
  lE eq _        _        = False

-- | Infix of 'lE'.
-- Checks equality trough a type using a custom equality test.
(=^) ::
  ( Eq1 f
  )
    => f a -> f b -> (a -> b -> Bool) -> Bool
(a =^ b) eq = lE eq a b

-- | Checks inequality trough a type using a custom equality test.
(/=^) ::
  ( Eq1 f
  )
    => f a -> f b -> (a -> b -> Bool) -> Bool
(a /=^ b) eq = not ( lE eq a b )

class Eq1 f => Ord1 f where
  lC :: (a -> b -> Ordering) -> f a -> f b -> Ordering

instance
  ( Ord1 f
  , Ord1 g
  )
    => Ord1 (Compose f g) where
  lC cmp (C x) (C y) = lC (lC cmp) x y

instance
  ( Ord1 f
  , Ord1 (p a)
  )
    => Ord1 (Bicompose f p a) where
  lC cmp (Bc x) (Bc y) = lC (lC cmp) x y

instance
  ( Ord1 f
  , Functor f
  , Ord1 (Flip p a)
  )
    => Ord1 (Flip (Bicompose f p) a) where
  lC cmp (F (Bc x)) (F (Bc y)) = lC (lC cmp) (m F x) (m F y)

instance
  ( Ord1 f
  , Ord1 (p (f a))
  )
    => Ord1 (On p f a) where
  lC cmp (On x) (On y) = lC (lC cmp) x y

instance
  ( Ord1 f
  , Ord1 (Flip p (f a))
  )
    => Ord1 (Flip (On p f) a) where
  lC cmp (F (On x)) (F (On y)) = lC (lC cmp) (F x) (F y)

instance
  ( Ord1 f
  , Ord1 g
  )
    => Ord1 (Sum f g) where
  lC cmp (Ir x) (Ir y) = lC cmp x y
  lC cmp (Il x) (Il y) = lC cmp x y
  lC _   (Il x) (Ir y) = LT
  lC _   (Ir x) (Il y) = GT

instance
  ( Ord1 (p a)
  , Ord1 (q a)
  )
    => Ord1 (Bisum p q a) where
  lC cmp (IR x) (IR y) = lC cmp x y
  lC cmp (IL x) (IL y) = lC cmp x y
  lC _   (IL x) (IR y) = LT
  lC _   (IR x) (IL y) = GT

instance
  ( Ord1 (Flip p a)
  , Ord1 (Flip q a)
  )
    => Ord1 (Flip (Bisum p q) a) where
  lC cmp (F (IR x)) (F (IR y)) = lC cmp (F x) (F y)
  lC cmp (F (IL x)) (F (IL y)) = lC cmp (F x) (F y)
  lC _   (F (IL x)) (F (IR y)) = LT
  lC _   (F (IR x)) (F (IL y)) = GT

instance Ord1 [] where
  lC cmp []       []       = EQ
  lC cmp (x : xs) (y : ys) =
    case
      cmp x y
    of
      EQ ->
        lC cmp xs ys
      other ->
        other
  lC cmp (x : xs) []       = GT
  lC cmp []       (y : ys) = LT

-- | '(Prelude.>)' using a lifted comparison.
(>^) ::
  ( Ord1 f
  )
    => f a -> f b -> (a -> b -> Ordering) -> Bool
(a >^ b) cmp = lC cmp a b == GT

-- | '(Prelude.<)' using a lifted comparison.
(<^) ::
  ( Ord1 f
  )
    => f a -> f b -> (a -> b -> Ordering) -> Bool
(a <^ b) cmp = lC cmp a b == LT

-- | '(Prelude.>=)' using a lifted comparison.
(>=^) ::
  ( Ord1 f
  )
    => f a -> f b -> (a -> b -> Ordering) -> Bool
(a >=^ b) cmp = lC cmp a b /= LT

-- | '(Prelude.<=)' using a lifted comparison.
(<=^) ::
  ( Ord1 f
  )
    => f a -> f b -> (a -> b -> Ordering) -> Bool
(a <=^ b) cmp = lC cmp a b /= GT

