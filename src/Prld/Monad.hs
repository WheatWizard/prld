{-# Language ConstraintKinds #-}
{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
{-# Language MultiParamTypeClasses #-}
{-# Language QuantifiedConstraints #-}
{-# Language FunctionalDependencies #-}
module Prld.Monad
  ( module Prld.Functor
  , Pure
    ( p
    )
  , pp, fp
  , Bipure
    ( bp
    )
  , Apply
    ( (<*>)
    , l2
    )
  , l3, l4, l5, l6, l7
  , Applicative
  , Bind
    ( j
    , (>>=)
    )
  , (=<<)
  , Monad
  )
  where

import Prld.Functor
import Prld.Flip

import qualified Control.Applicative as Applicative
import qualified Control.Monad as Monad

import qualified Prelude
import Prelude
  ( IO
  )


-- | 'Pure' is a class for 'Functor's with a pure.
-- Like 'Prelude.Applicative' without '(<*>)' or 'Prelude.liftA2'.
class Functor f => Pure f where
  -- | The pure.
  p :: a -> f a

instance Pure IO where
  p = Prelude.pure

instance Pure ((->) a) where
  p x y = x 

instance Pure [] where
  p = (: [])

instance
  ( Bipure p
  )
    => Pure (Join p) where
  p x = W (bp x x)

instance
  ( Pure f
  , Pure g
  )
    => Pure (Compose f g) where
  p = C . p . p

instance
  ( Pure f
  , Pure g
  )
    => Pure (Product f g) where
  p x = Pr (p x) (p x)

-- | A double pure.
-- Equivalent to @'p' '.' 'p'@
pp ::
  ( Pure f
  , Pure g
  )
    => a -> f (g a)
pp = p . p

-- | Take two arguments and returns the second.
-- Equivalent to @'f' 'p'@
fp :: a -> b -> b
fp = f p

-- | A generalized version of more traditional @Apply@ classes.
-- Instances should generally be of the form
--
-- > Apply a a a
--
-- However more broad instances are supported.
--
-- Instances of the form
--
-- > Apply a b c
--
-- Should generally be such that @a@, @b@ and @c@ are *similar* types.
--
-- For example '[]' and 'Maybe' are similar in that `Maybe` is essentially just a list limited to fewer than 2 items.
--
-- Some normal properties may only make sense for instances of the form @Apply a a a@, it is expected that instances of other forms just behave in a reasonable way.
--
-- Instances should satisfy the properties:
--
-- prop> (((.) . u) <*> v) <*> w = u <*> (v <*> w)
-- prop> x <*> (f . y) = ((. f) . x) <*> y
-- prop> f . (x <*> y) = ((f .) . x) <*> y
class Functor f => Apply f g h | f g -> h where
  {-# Minimal (<*>) | l2 #-}
  (<*>) :: f (a -> b) -> g a -> h b
  (<*>) = l2 Prelude.id
  infixl 4 <*>
  l2 :: (a -> b -> c) -> f a -> g b -> h c
  l2 f m1 m2 = f . m1 <*> m2

-- | Lifts a function of 3 arguments to an applicative.
l3 ::
  ( Apply f g j
  , Apply j h i
  ) => (a -> b -> c -> d) -> f a -> g b -> h c -> i d 
l3 f a b c = l2 f a b <*> c

-- | Lifts a function of 4 arguments to an applicative.
l4 ::
  ( Apply f g z
  , Apply z h y
  , Apply y i j
  )
    => (a -> b -> c -> d -> e) -> f a -> g b -> h c -> i d -> j e
l4 f a b c d = l3 f a b c <*> d

-- | Lifts a function of 5 arguments to an applicative.
l5 ::
  ( Apply f1 f2 g1
  , Apply g1 f3 g2
  , Apply g2 f4 g3
  , Apply g3 f5 f6
  )
    => (a1 -> a2 -> a3 -> a4 -> a5 -> a6) -> f1 a1 -> f2 a2 -> f3 a3 -> f4 a4 -> f5 a5 -> f6 a6
l5 f a b c d e = l4 f a b c d <*> e

-- | Lifts a function of 6 arguments to an applicative.
l6 ::
  ( Apply f1 f2 g1
  , Apply g1 f3 g2
  , Apply g2 f4 g3
  , Apply g3 f5 g4
  , Apply g4 f6 f7
  )
    => (a1 -> a2 -> a3 -> a4 -> a5 -> a6 -> a7) -> f1 a1 -> f2 a2 -> f3 a3 -> f4 a4 -> f5 a5 -> f6 a6 -> f7 a7
l6 func a b c d e f = l5 func a b c d e <*> f

-- | Lifts a function of 7 arguments to an applicative.
l7 ::
  ( Apply f1 f2 g1
  , Apply g1 f3 g2
  , Apply g2 f4 g3
  , Apply g3 f5 g4
  , Apply g4 f6 g5
  , Apply g5 f7 f8
  )
    => (a1 -> a2 -> a3 -> a4 -> a5 -> a6 -> a7 -> a8) -> f1 a1 -> f2 a2 -> f3 a3 -> f4 a4 -> f5 a5 -> f6 a6 -> f7 a7 -> f8 a8
l7 func a b c d e f g = l6 func a b c d e f <*> g

instance Apply IO IO IO where
  (<*>) = (Prelude.<*>)
  l2 = Applicative.liftA2

instance Apply ((->) a) ((->) a) ((->) a) where
  (<*>) = (Prelude.<*>)
  l2 = Applicative.liftA2

instance Apply [] [] [] where
  (<*>) = (Prelude.<*>)
  l2 = Applicative.liftA2

{-
instance MSet a b => Apply ((,) a) ((,) b) ((,) a) where
  (a, f) <*> (b, x) = (a <> b, f x)
-}

-- | 'Bipure' is a class for 'Bifunctor's with a bipure.
-- Like @Biapplicative@ without a biapply.
class (forall a . Functor (p a), forall a . Functor (Flip p a)) => Bipure p where
  bp :: a -> b -> p a b

instance Bipure (,) where
  bp a b = (a, b)

-- | Shadows 'Prelude.Applicative'.
-- '(Prelude.<*>)' and 'Prelude.liftA2' are now defined in 'Apply' as 
-- '(<*>)' and 'l2'.
-- 'Prelude.pure' is defined in 'Pure' as
-- 'p'.
type Applicative f = (Pure f, Apply f f f)

-- | 'Apply's with a join are 'Bind'.
-- This can also be thought of as a 'Monad' without a pure.
--
-- The following law should hold.
--
-- prop> f <*> m = f >>= (. m)
class (Apply f g h, Functor g) => Bind f g h | f g -> h where
  {-# Minimal (>>=) | j #-}
  -- | The join
  j :: f (g a) -> h a
  j = (>>= Prelude.id)
  -- | The bind
  (>>=) :: f a -> (a -> g b) -> h b
  m >>= f = j (f . m)

-- | flip of '(>>=)'.
-- Equivalent to @'f'(>>=)@.
(=<<) :: Bind f g h => (a -> g b) -> f a -> h b
f =<< m = m >>= f

instance Bind IO IO IO where
  j     = Monad.join
  (>>=) = (Prelude.>>=)

instance Bind ((->) a) ((->) a) ((->) a) where
  j     = Monad.join
  (>>=) = (Prelude.>>=)

instance Bind [] [] [] where
  j     = Monad.join
  (>>=) = (Prelude.>>=)

-- | Shadows 'Prelude.Monad'.
-- '(Prelude.>>=)' and 'Prelude.join' are now defined in 'Bind' as 
-- '(>>=)' and 'j'.
-- 'Prelude.return' is defined in 'Pure' as
-- 'p'.
type Monad m = (Pure m, Bind m m m)

